<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>wjfcms-laravel - @yield('title','后台管理系统')</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- controller name -->
    <meta name="control_name" content="@yield('control_name','index')">
    <link rel="stylesheet" href="{{ asset('static/layuiadmin/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('static/layuiadmin/style/admin.css') }}" media="all">
    <script src="{{ asset('static/layuiadmin/layui/layui.js') }}"></script>
    <script src="{{ asset('js/echarts/echarts.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" media="all">
    <script src="{{ asset('js/echarts/qihuo.js') }}"></script>
    @yield('header')
    <style>
        .layui-chart {
            margin-top: 50px;
        }

        #varietie_codes .layui-icon {
            position: static;
        }
    </style>
</head>

<body>

    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <div style="padding-left: 15px;">
                <h1 style="display: inline-block">{{ $site_name }}</h1>
            </div>

            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
                <li class="layui-nav-item" lay-unselect>
                    <script type="text/html" template lay-url="" lay-done="layui.element.render('nav', 'layadmin-layout-right');">
                        <a href="/admin">
                            <cite>登录</cite>
                        </a>
                    </script>
                </li>
            </ul>
        </div>

        <div class="layui-chart">
            <div class="layui-fluid">
                <div class="layui-card" id="layui-card">
                    <!-- 搜索 -->
                    @component('./layouts/public_search',['exchange_list' => $exchange_list,'exchange_id'=>$exchange_id,'varietie_codes'=>$varietie_codes,'varietie_code'=>$varietie_code,'start_time'=>$start_time,'end_time'=>$end_time])
                    @endcomponent
                </div>
                <div class="layui-card" id="layui-card" style="width: 100%;height: 100%">
                    <div id="main" style="width: 100%;height:400px;"></div>
                </div>
            </div>
        </div>

    </div>

    <script>
        layui.config({
            base: "/static/layuiadmin/"
        }).extend({
            index: 'lib/index'
        }).use(['index', 'table', 'admin', 'laydate'], function() {
            var table = layui.table,
                $ = layui.$,
                admin = layui.admin,
                laydate = layui.laydate,
                form = layui.form;
            var control_name = $('meta[name="control_name"]').attr('content');
            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            var exchange_id = '';
            var varietie_code = '';
            var times = '';

            //日期范围
            laydate.render({
                elem: '#times',
                range: '~',
                max: 0 //0天后
            });
            // 监听交易所切换
            form.on('select(exchange_id)', function(data) {
                var html = '';
                $.get('/home/getVarietieList?exchange_id=' + data.value).done(function(res) {
                    for (let i in res.data) {
                        if (i == 0) {
                            html += '<input type="radio" name="varietie_code" title="' + res.data[i] + '" value="' + res.data[i] + '" checked>';
                        } else {
                            html += '<input type="radio" name="varietie_code" title="' + res.data[i] + '" value="' + res.data[i] + '">';
                        }
                    }
                    $('#varietie_codes').html(html)
                    form.render();
                });
            });
            //监听搜索
            form.on('submit(LAY-app-search)', function(data) {
                var field = data.field;
                exchange_id = field.exchange_id;
                if (field.varietie_code === undefined || field.varietie_code == '') {
                    varietie_code = '';
                } else {
                    varietie_code = field.varietie_code;
                }

                times = field.times;
                reqData();
            });
            var theme = 'qihuo';
            // 基于准备好的dom，初始化echarts实例
            var myChart = echarts.init(document.getElementById('main'), theme);

            function reqData() {
                myChart.showLoading();
                $.get('/getChart?times=' + times + '&exchange_id=' + exchange_id + '&varietie_code=' + varietie_code).done(function(res) {
                    // 品种代码
                    var varietie_code_data = res.varietie_code;
                    //时间轴
                    var time_data = res.time_data;
                    var series_data = res.data;
                    // 指定图表的配置项和数据
                    var option = {
                        title: {
                            text: '收盘价走势图',
                            subtext: '数据来自中国期货市场监控中心',
                            left: 'center',
                            align: 'right'
                        },
                        tooltip: {
                            trigger: 'axis'
                        },
                        legend: {
                            data: varietie_code_data,
                            left: 10
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        toolbox: {
                            feature: {
                                saveAsImage: {}
                            }
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: time_data
                        },
                        yAxis: {
                            type: 'value',
                            smooth: true
                        },
                        series: series_data
                    };
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.hideLoading();
                    myChart.setOption(option, true);
                });
            }

            reqData();
        });
    </script>

</body>

</html>