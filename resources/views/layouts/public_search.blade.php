<!-- 搜索 -->
<div class="layui-form layui-card-header layuiadmin-card-header-auto">
    <div class="layui-form-item">
        @if(isset($is_admin) && $is_admin === true)
        <div class="layui-inline">
            <label class="layui-form-label">交易账号</label>
            <div class="layui-input-inline">
                <select name="admin_id" lay-filter="admin_id" lay-search="">
                    @foreach($transaction_users as $k=>$v)
                    <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        <div class="layui-inline">
            <label class="layui-form-label">交易时间</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="times" placeholder=" ~ " name="times" value="{{ $start_time }} ~ {{ $end_time }}">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">所属交易所</label>
            <div class="layui-input-inline">
                <select name="exchange_id" lay-filter="exchange_id">
                    @foreach($exchange_list as $k=>$v)
                    <option @if($v['id']==$exchange_id) selected @endif value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">品种代码</label>
            <div class="layui-input-block" id="varietie_codes">
                @foreach($varietie_codes as $key=>$val)
                <input type="radio" name="varietie_code" @if($val==$varietie_code) checked @endif title="{{ $val }}（{{ $key }}）" value="{{ $val }}">
                @endforeach
            </div>
        </div>
        <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
        </div>
    </div>
</div>