@extends('layouts.admin')
@section('control_name', $control_name)
@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <!-- 搜索 -->
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">名称/代码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="title" placeholder="请输入品种名称或品种代码" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">所属交易所</label>
                    <div class="layui-input-inline">
                        <select name="exchange_id">
                            <option value="">请选择交易所</option>
                            @foreach($exchange_list as $k=>$v)
                            <option value="{{ $v->id }}">{{ $v->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">是否删除</label>
                    <div class="layui-input-inline">
                        <select name="delete">
                            @foreach($delete_list as $dk=>$dv)
                            <option value="{{ $dk }}">{{ $dv }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="layui-card-body" style="min-height: 600px">
            <!-- 按钮组 -->
            <div style="padding-bottom: 10px;">
                <button type="button" class="layui-btn" id="up_file">
                    <i class="layui-icon">&#xe67c;</i>导入数据
                </button>
                <!-- <button class="layui-btn layuiadmin-btn-list layui-btn-warm" data-type="example_xlsx">上传示例表格
                </button>
                <button class="layui-btn layuiadmin-btn-list layui-btn-danger" data-type="batchdel">删除</button> -->
                <button class="layui-btn layuiadmin-btn-list" data-type="add">添加交易品种数据</button>
            </div>
            <!-- 表格 -->
            <table id="LAY-app-list" lay-filter="LAY-app-list"></table>
            <!-- 模板渲染 -->
            <script type="text/html" id="statusTpl">
                @{{# if(d.deleted_at == null){ }}
                    <input type="checkbox" name="status" lay-skin="switch" lay-filter="table-button-status" data-id="@{{ d.id }}" lay-text="已审核|待审核" @{{ d.status?'checked':'' }}>
                    @{{# } }}
            </script>
            <!-- 模板渲染 -->
            <script type="text/html" id="table-list">
                @{{# if(d.deleted_at == null){ }}
                    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">
                        <i class="layui-icon layui-icon-edit"></i>编辑
                    </a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
                        <i class="layui-icon layui-icon-delete"></i>删除
                    </a>
                    @{{# } else { }}
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="restore">
                            <i class="layui-icon layui-icon-refresh-3"></i>恢复数据</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="force_del">
                            <i class="layui-icon layui-icon-fonts-del"></i>彻底删除</a>
                        @{{# } }}
            </script>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'admin', 'laydate', 'upload'], function() {
        var table = layui.table,
            $ = layui.$,
            admin = layui.admin,
            upload = layui.upload,
            laydate = layui.laydate,
            form = layui.form;
        var control_name = $('meta[name="control_name"]').attr('content');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });

        //表格数据
        table.render({
            elem: '#LAY-app-list',
            url: '/admin/' + control_name + '/index',
            method: 'post',
            where: {},
            headers: {
                'X-CSRF-TOKEN': csrf_token
            },
            cols: [
                [{
                    type: 'checkbox',
                    fixed: 'left'
                }, {
                    field: 'id',
                    width: 100,
                    title: 'ID',
                    align: 'center',
                    sort: true
                }, {
                    field: 'title',
                    title: '品种名称',
                    align: 'center',
                }, {
                    field: 'code',
                    title: '品种代码',
                    align: 'center',
                }, {
                    field: 'exchange_name',
                    title: '交易所',
                    align: 'center',
                }, {
                    field: 'status',
                    title: '状态',
                    templet: '#statusTpl',
                    minWidth: 80,
                    align: 'center'
                }, {
                    field: 'created_at',
                    title: '提交时间',
                    sort: true
                }, {
                    title: '操作',
                    minWidth: 200,
                    align: 'center',
                    fixed: 'right',
                    toolbar: '#table-list'
                }]
            ],
            page: true,
            limit: 10,
            done: function(res, curr, count) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
            }
        });

        //监听指定开关
        form.on('switch(table-button-status)', function(data) {
            var field = {
                status: this.checked ? 1 : 0,
                id: $(this).data('id')
            };
            admin.req({
                url: '/admin/' + control_name + '/open',
                data: field,
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                beforeSend: function(XMLHttpRequest) {
                    layer.load();
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        layer.msg(res.msg, {
                            offset: '15px',
                            icon: 1,
                            time: 1000
                        }, function() {});
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
        });

        var uploadInst = upload.render({
            elem: '#up_file',
            url: '/admin/closing_data/import',
            headers: {
                'X-CSRF-TOKEN': csrf_token
            },
            accept: 'file',
            field: "file",
            type: 'file',
            exts: 'xls|xlsx' //设置一些后缀，用于演示前端验证和后端的验证
                ,
            before: function(obj) {
                //预读本地文件示例，不支持ie8
                this.data = {
                    'ca_type': 'varieties'
                }; //关键代码
            },
            done: function(res) {
                //如果上传失败
                if (res.code > 0) {
                    return layer.msg('上传失败', {
                        icon: 2
                    });
                }
                //上传成功
                layer.msg(res.msg, {
                    icon: 1
                });
                table.reload('LAY-app-list');
            },
            error: function() {
                //演示失败状态，并实现重传
                var up_logo_text = $('#up_logo_text');
                up_logo_text.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                up_logo_text.find('.demo-reload').on('click', function() {
                    uploadInst.upload();
                });
            }
        });

        //监听工具条
        table.on('tool(LAY-app-list)', function(obj) {
            if (obj.event === 'del') {
                layer.confirm('确定删除数据吗?', function(index) {
                    admin.req({
                        url: '/admin/' + control_name + '/destroy',
                        data: {
                            id: obj.data.id
                        },
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': csrf_token
                        },
                        beforeSend: function(XMLHttpRequest) {
                            layer.load();
                        },
                        done: function(res) {
                            layer.closeAll('loading');
                            if (res.code === 0) {
                                layer.msg(res.msg, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    obj.del();
                                    layer.close(index);
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            }
                        }
                    });
                });
            } else if (obj.event === 'edit') {
                layer.open({
                    type: 2,
                    title: '编辑',
                    content: '/admin/' + control_name + '/edit/' + obj.data.id,
                    maxmin: true,
                    area: ['450px', '500px'],
                    btn: ['确定', '取消'],
                    yes: function(index, layero) {
                        var iframeWindow = window['layui-layer-iframe' + index],
                            submit = layero.find('iframe').contents().find("#layuiadmin-app-form-edit");

                        //监听提交
                        iframeWindow.layui.form.on('submit(layuiadmin-app-form-edit)', function(data) {
                            var field = data.field; //获取提交的字段
                            //提交 Ajax 成功后，静态更新表格中的数据
                            admin.req({
                                url: '/admin/' + control_name + '/update',
                                data: field,
                                method: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': csrf_token
                                },
                                beforeSend: function(XMLHttpRequest) {
                                    layer.load();
                                },
                                done: function(res) {
                                    layer.closeAll('loading');
                                    if (res.code === 0) {
                                        layer.msg(res.msg, {
                                            offset: '15px',
                                            icon: 1,
                                            time: 1000
                                        }, function() {
                                            obj.update({
                                                sort_order: field.sort_order,
                                                name: field.name,
                                                guard_name: field.guard_name,
                                                url: field.url,
                                                icon: field.icon,
                                                display_menu: field.display_menu
                                            });
                                            table.reload('LAY-app-list');
                                            layer.close(index); //关闭弹层
                                        });
                                    } else {
                                        layer.msg(res.msg, {
                                            icon: 2
                                        });
                                    }

                                }
                            });
                        });
                        submit.trigger('click');
                    }
                });
            } else if (obj.event === 'restore') {
                layer.confirm('确定恢复数据吗?', function(index) {
                    admin.req({
                        url: '/admin/' + control_name + '/restore',
                        data: {
                            id: obj.data.id
                        },
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': csrf_token
                        },
                        beforeSend: function(XMLHttpRequest) {
                            layer.load();
                        },
                        done: function(res) {
                            layer.closeAll('loading');
                            if (res.code === 0) {
                                layer.msg(res.msg, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    table.reload('LAY-app-list');
                                    layer.close(index); //关闭弹层
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            }
                        }
                    });
                });
            } else if (obj.event === 'force_del') {
                layer.confirm('确定要彻底删除数据吗?', function(index) {
                    admin.req({
                        url: '/admin/' + control_name + '/forceDelete',
                        data: {
                            id: obj.data.id
                        },
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': csrf_token
                        },
                        beforeSend: function(XMLHttpRequest) {
                            layer.load();
                        },
                        done: function(res) {
                            layer.closeAll('loading');
                            if (res.code === 0) {
                                layer.msg(res.msg, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    obj.del();
                                    layer.close(index);
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            }
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            var field = data.field;
            //执行重载
            table.reload('LAY-app-list', {
                where: field
            });
        });

        //按钮组
        var $ = layui.$,
            active = {
                batchdel: function() {
                    return layer.msg('暂不支持交易所批量删除', {
                        icon: 2
                    });
                    var checkStatus = table.checkStatus('LAY-app-list'),
                        checkData = checkStatus.data; //得到选中的数据
                    if (checkData.length === 0) {
                        return layer.msg('请选择数据', {
                            icon: 2
                        });
                    }
                    var ids = [];
                    for (i in checkData) {
                        ids.push(checkData[i].id);
                    }
                    layer.confirm('确定批量删除吗？', function(index) {
                        admin.req({
                            url: '/admin/' + control_name + '/destroy',
                            data: {
                                id: ids
                            },
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': csrf_token
                            },
                            beforeSend: function(XMLHttpRequest) {
                                layer.load();
                            },
                            done: function(res) {
                                layer.closeAll('loading');
                                if (res.code === 0) {
                                    layer.msg(res.msg, {
                                        offset: '15px',
                                        icon: 1,
                                        time: 1000
                                    }, function() {
                                        table.reload('LAY-app-list');
                                        layer.close(index); //关闭弹层
                                    });
                                } else {
                                    layer.msg(res.msg, {
                                        icon: 2
                                    });
                                }
                            }
                        });
                    });
                },
                add: function() {
                    layer.open({
                        type: 2,
                        title: '添加',
                        content: '/admin/' + control_name + '/create',
                        maxmin: true,
                        area: ['450px', '500px'],
                        btn: ['确定', '取消'],
                        yes: function(index, layero) {
                            var iframeWindow = window['layui-layer-iframe' + index],
                                submit = layero.find('iframe').contents().find("#layuiadmin-app-form-add");
                            //监听提交
                            iframeWindow.layui.form.on('submit(layuiadmin-app-form-add)', function(data) {
                                var field = data.field;
                                admin.req({
                                    url: '/admin/' + control_name + '/store',
                                    data: field,
                                    method: 'POST',
                                    headers: {
                                        'X-CSRF-TOKEN': csrf_token
                                    },
                                    beforeSend: function(XMLHttpRequest) {
                                        layer.load();
                                    },
                                    done: function(res) {
                                        layer.closeAll('loading');
                                        if (res.code === 0) {
                                            layer.msg(res.msg, {
                                                offset: '15px',
                                                icon: 1,
                                                time: 1000
                                            }, function() {
                                                table.reload('LAY-app-list');
                                                layer.close(index); //关闭弹层
                                            });
                                        } else {
                                            layer.msg(res.msg, {
                                                icon: 2
                                            });
                                        }
                                    }
                                });
                            });
                            submit.trigger('click');
                        }
                    });
                },
                example_xlsx: function() {
                    let html = '<img src="/images/file/varieties_xlsx.png" alt="示例图表">';
                    layer.open({
                        title: '上传数据格式',
                        type: 1,
                        content: html,
                        area: ['650px', '300px']
                    });
                }
            };
        $('.layui-btn.layuiadmin-btn-list').on('click', function() {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });
</script>
@endsection