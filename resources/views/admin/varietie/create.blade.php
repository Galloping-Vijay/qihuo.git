@extends('layouts.admin')
@section('control_name', $control_name)
@section('content')
    <div class="layui-form" lay-filter="layuiadmin-app-list" id="layuiadmin-app-form-list"
         style="padding: 20px 30px 0 0;">
        <div class="layui-form-item">
            <label class="layui-form-label">所属交易所</label>
            <div class="layui-input-inline">
                <select name="exchange_id">
                    @foreach($exchange_list as $k=>$v)
                        <option value="{{ $v->id }}">{{ $v->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline">
                <input type="text" name="title" lay-verify="required" placeholder="请输入" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">代码</label>
            <div class="layui-input-inline">
                <input type="text" name="code" placeholder="请输入" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <!-- <div class="layui-form-item">
            <label class="layui-form-label">交易价</label>
            <div class="layui-input-inline">
                <input type="text" name="transaction" placeholder="请输入" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">收盘价</label>
            <div class="layui-input-inline">
                <input type="text" name="closing_quotation" placeholder="请输入" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手续费</label>
            <div class="layui-input-inline">
                <input type="text" name="handling_fee" placeholder="请输入" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">品种日期</label>
            <div class="layui-input-inline">
                <input id="date_time" type="text" name="date_time" placeholder="yyyy-MM-dd" autocomplete="off"
                       class="layui-input">
            </div>
        </div> -->
        <div class="layui-form-item">
            <label class="layui-form-label">描述</label>
            <div class="layui-input-inline">
                <textarea name="description" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="hidden" name="status" value="1">
                <input type="checkbox" checked lay-filter="status" lay-skin="switch"
                       lay-text="已审核|待审核">
            </div>
        </div>
        <div class="layui-form-item layui-hide">
            <input type="button" lay-submit lay-filter="layuiadmin-app-form-add" id="layuiadmin-app-form-add"
                   value="确认添加">
            <input type="button" lay-submit lay-filter="layuiadmin-app-form-edit" id="layuiadmin-app-form-edit"
                   value="确认编辑">
        </div>
    </div>
@endsection

@section('footer')
@endsection

@section('script')
    <script>
        layui.config({
            base: "/static/layuiadmin/"
        }).extend({
            index: 'lib/index'
        }).use(['index', 'table', 'admin','laydate'], function () {
            var $ = layui.$
                , admin = layui.admin
                , laydate = layui.laydate
                , form = layui.form;
            laydate.render({
                elem: '#date_time'
                ,trigger: 'click'
            });
            //监听指定开关
            form.on('switch(status)', function () {
                if (this.checked) {
                    $("input[name='status']").val('1');
                } else {
                    $("input[name='status']").val('0');
                }
            });
        });
    </script>
@endsection