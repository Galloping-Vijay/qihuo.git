@extends('layouts.admin')
@section('control_name', $control_name)
@section('content')
<div class="layui-fluid" id="LAY-app-message-detail">
  <div class="layui-card layuiAdmin-msg-detail">
    <div>
      <div class="layui-card-header">
        <h1>{{ $info['title'] }}</h1>
        <p>
          <span>{{ $info['created_at'] }}</span>
        </p>
      </div>
      <div class="layui-card-body layui-text">
        <div class="layadmin-text">
           {{  $info['text'] }}
        </div>

        <div style="padding-top: 30px;">
          <a lay-href="/admin/message/index?status_change=1" class="layui-btn layui-btn-primary layui-btn-sm">返回上级</a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
<script>
    var control_name = document.querySelector('meta[name="control_name"]').getAttribute('content');
    var csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'form'], function () {
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,element = layui.element;
    });
</script>
@endsection