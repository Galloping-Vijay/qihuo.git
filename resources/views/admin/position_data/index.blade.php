@extends('layouts.admin')
@section('control_name', $control_name)
@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <!-- 搜索 -->
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                @if(isset($is_admin) && $is_admin === true)
                <div class="layui-inline">
                    <label class="layui-form-label">所属账号</label>
                    <div class="layui-input-inline">
                        <select name="admin_id" lay-filter="admin_id" lay-search="">
                            <option value="">全部</option>
                            @foreach($admin_user as $k=>$v)
                            <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                <div class="layui-inline">
                    <label class="layui-form-label">交易时间</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" id="times" placeholder=" ~ " name="times" value="">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">名称/代码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="keyword" placeholder="请输入" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">所属交易所</label>
                    <div class="layui-input-inline">
                        <select name="exchange_code">
                            <option value="">请选择交易所</option>
                            @foreach($exchange_list as $k=>$v)
                            <option value="{{ $v->code }}">{{ $v->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="layui-card-body" style="min-height: 600px">
            <!-- 按钮组 -->
            <!-- 表格 -->
            <table id="LAY-app-list" lay-filter="LAY-app-list"></table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'admin', 'laydate', 'upload'], function() {
        var table = layui.table,
            $ = layui.$,
            upload = layui.upload,
            admin = layui.admin,
            laydate = layui.laydate,
            form = layui.form;
        var control_name = $('meta[name="control_name"]').attr('content');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });
        //日期范围
        laydate.render({
            elem: '#times',
            range: '~'
            ,max: 0 //0天后
        });

        //表格数据
        table.render({
            elem: '#LAY-app-list',
            url: '/admin/' + control_name + '/index',
            method: 'post',
            where: {},
            headers: {
                'X-CSRF-TOKEN': csrf_token
            },
            cols: [
                [
                    {
                        field: 'id',
                        width: 75,
                        title: 'ID',
                        align: 'center',
                        sort: true,
                    }, {
                        field: 'admin_name',
                        title: '客户名称',
                        align: 'center',
                        minWidth:80,
                    }, {
                        field: 'contract',
                        title: '合约',
                        align: 'center',
                        width: 90,
                    }, {
                        field: 'deal_time',
                        title: '交易时间',
                        align: 'center',
                        width: 180,
                    }, {
                        field: 'buy_position',
                        title: '买持仓',
                        align: 'center',
                        minWidth:80,
                    }, {
                        field: 'buy_average_price',
                        title: '买均价',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'sell_position',
                        title: '卖持仓',
                        align: 'center',
                        minWidth:80,
                    }, {
                        field: 'sell_average_price',
                        title: '卖均价',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'settlement_price_yesterday',
                        title: '昨结算价',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'settlement_price_today',
                        title: '今结算价',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'profit_loss',
                        title: '平仓盈亏',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'trading_margin',
                        title: '交易保证金',
                        align: 'center',
                        minWidth:120,
                    }, {
                        field: 'speculation',
                        title: '投机/套保',
                        align: 'center',
                    }
                ]
            ],
            page: true,
            limit: 20,
            done: function(res, curr, count) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
            }
        });
        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            var field = data.field;
            //执行重载
            table.reload('LAY-app-list', {
                where: field
            });
        });
    });
</script>
@endsection