<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>证券期货大数据分析系统</title>

    <link href="{{ asset('css/home/default.css') }}" rel="stylesheet" type="text/css">
    <!--必要样式-->
    <link href="{{ asset('css/home/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/home/demo.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/home/loaders.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('js/home/jquery-2.1.1.min.js') }}"></script>

    <link id="layuicss-skinlayercss" rel="stylesheet" href="{{ asset('css/home/layer.css') }}" media="all">
    <style>
        #captcha_src_class{
            margin: -2px -7px 0px;
        }
    </style>
</head>

<body><canvas class="pg-canvas" width="1920" height="1080"></canvas>
    <div class="login">
        <div class="login_title">
            <p><img src="{{ asset('images/home/logo.png') }}" /></p>
            <span>管理员注册</span>
        </div>
        <form id="form">
            <div class="login_fields">
                <div class="login_fields__user">
                    <div class="icon" style="opacity: 0.5;">
                        <img alt="" src="{{ asset('images/home/user_icon_copy.png') }}">
                    </div>
                    @csrf
                    <input name="account" placeholder="手机号" maxlength="16" class="username" type="text" autocomplete="off" value="" lay-verify="required" id="account">
                    <div class="validation" style="opacity: 1; right: 30px;">
                        <img alt="" src="{{ asset('images/home/tick.png') }}">
                    </div>
                </div>
                <div class="login_fields__password">
                    <div class="icon">
                    
                    </div>
                    <input name="vercode" placeholder="短信验证码" maxlength="4" class="ValidateNum" type="text" autocomplete="off" lay-verify="required" id="vercode">
                    <div class="yanz" style="opacity: 1233;right: 8px;top: -4px; ">
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-fluid" id="LAY-user-forget-getsmscode">获取验证码</button>
                    </div>
                </div>
                <div class="login_fields__password">
                    <div class="icon" style="opacity: 0.5;">
                        <img alt="" src="{{ asset('images/home/lock_icon_copy.png') }}">
                    </div>
                    <input name="password" class="passwordNumder" placeholder="密码" maxlength="16" type="password" autocomplete="off" lay-verify="required" id="password">
                    <div class="validation">
                        <img alt="" src="{{ asset('images/home/tick.png') }}">
                    </div>
                </div>
                <div class="login_fields__password">
                    <div class="icon">
                        <img alt="" src="{{ asset('images/home/key.png') }}">
                    </div>
                    <input name="captcha" placeholder="验证码" maxlength="4" class="ValidateNum" type="text" autocomplete="off" lay-verify="required" id="captcha">

                    <div class="yanz" style="opacity: 1233;right: -5px;top: -3px; ">
                        <img src="{{captcha_src()}}" class="layadmin-user-login-codeimg" onclick="this.src='{{captcha_src()}}'+Math.random()" id="captcha_src_class">
                    </div>
                </div>
                <div class="login_fields__submit">
                    <input type="button" value="注册" id="registerReq">
                    <input type="button" value="登录"  onclick="location.href='/admin/login'">
                </div>
            </div>
        </form>
        <div class="success">
        </div>
        <div class="disclaimer">
            <p>欢迎登陆{{ $systemConfig['site_name'] }}</p>
        </div>
    </div>
    <div class="authent">
        <div class="loader" style="height: 60px;width: 60px;margin-left: 28px;margin-top: 40px">
            <div class="loader-inner ball-clip-rotate-multiple">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <p>认证中...</p>
    </div>
    <div class="OverWindows"></div>
    <link href="{{ asset('css/home/layui.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ asset('js/home/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/home/stopExecutionOnTimeout.js') }}"></script>
    <script src="{{ asset('js/home/layui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/home/Particleground.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/home/Treatment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/home/jquery.mockjax.js') }}" type="text/javascript"></script>
    <div class="layui-layer-move"></div>
    <script src="{{ asset('static/layuiadmin/layui/layui.js') }}"></script>
    <script>
        layui.config({
            base: "/static/layuiadmin/" //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'user'], function() {
            var $ = layui.$,
                setter = layui.setter,
                admin = layui.admin,
                form = layui.form,
                router = layui.router(),
                search = router.search;
            //过期跳转
            if (top != self) {
                top.location.href = '/admin/login';
            }
            $('#submitReq').click(function() {
                let account = $('#account').val();
                let password = $('#password').val();
                let captcha = $('#captcha').val();
                if (!account || !password) {
                    layer.msg("<div style='color:black'>用户名和密码必填</div>", {
                        offset: '15px',
                        icon: 2,
                        time: 1000
                    }, function() {

                    });
                    return false;
                }
                if (!captcha) {
                    layer.msg("<div style='color:black'>验证码必填</div>", {
                        offset: '15px',
                        icon: 2,
                        time: 1000
                    }, function() {

                    });
                    return false;
                }
                let data = $('#form').serialize();
                $.ajax({
                    url: "{{ route('admin.login') }}",
                    type: 'post',
                    data: data,
                    success: function(res) {
                        //请求成功后，写入 access_token
                        layui.data(setter.tableName, {
                            key: setter.request.tokenName,
                            value: res.data.access_token
                        });
                        if (res.code === 0) {
                            layer.msg("<div style='color:black'>" + res.msg + "</div>", {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                location.href = '/admin/index/index'; //后台主页
                            });
                        } else {
                            layer.msg("<div style='color:black'>" + res.msg + "</div>", {
                                offset: '15px',
                                icon: 2,
                                time: 1000
                            }, function() {

                            });
                        }
                    }
                })
            })
        });
    </script>

</body>

</html>