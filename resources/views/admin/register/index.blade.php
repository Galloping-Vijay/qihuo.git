<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登入 -  {{ $systemConfig['site_name'] }}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{ asset('static/layuiadmin/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('static/layuiadmin/style/admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('static/layuiadmin/style/login.css') }}" media="all">
</head>
<body>
<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">
    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>{{ $systemConfig['site_name'] }}</h2>
            <p>{{ $systemConfig['seo_title'] }}</p>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            @csrf
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-cellphone" for="LAY-user-login-cellphone"></label>
                <input type="text" name="account" id="LAY-user-login-cellphone" lay-verify="phone" placeholder="手机" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-smscode"></label>
                        <input type="text" name="vercode" id="LAY-user-login-smscode" lay-verify="required" placeholder="短信验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-fluid" id="LAY-user-forget-getsmscode">获取验证码</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                <input type="password" name="password" id="LAY-user-login-password" lay-verify="pass" placeholder="密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-repass"></label>
                <input type="password" name="password_confirmation" id="LAY-user-login-repass" lay-verify="required" placeholder="确认密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-nickname"></label>
                <input type="text" name="username" id="LAY-user-login-nickname" lay-verify="username" placeholder="昵称" class="layui-input">
            </div>
{{--            <div class="layui-form-item">--}}
{{--                <input type="checkbox" name="agreement" lay-skin="primary" title="同意用户协议" checked>--}}
{{--            </div>--}}
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                        <input type="text" name="captcha" id="LAY-user-login-vercode" lay-verify="required" placeholder="图形验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <img src="{{captcha_src()}}" class="layadmin-user-login-codeimg" onclick="this.src='{{captcha_src()}}'+Math.random()" id="captcha_src_class">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-reg-submit">注 册</button>
            </div>
            <div class="layui-trans layui-form-item layadmin-user-login-other">
                <label>用已有帐号?</label>
                <a href="/admin/login" class="layadmin-user-jump-change layadmin-link layui-hide-xs">用已有帐号登入</a>
                <a href="/admin/login" class="layadmin-user-jump-change layadmin-link layui-hide-sm layui-show-xs-inline-block">登入</a>
            </div>
        </div>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

        <p>© 2017~2020 <a href="https://www.choudalao.com" target="_blank">{{ $systemConfig['site_copyright'] }}</a> 版权所有</p>
        <p>
            <span><a href="{{ config('vijay.document') }}" target="_blank">开发文档</a></span>
            <span><a href="https://www.choudalao.com" target="_blank">臭大佬首页</a></span>
            <span><a href="{{ config('vijay.github') }}" target="_blank">Github</a></span>
        </p>
    </div>

</div>

<script src="{{ asset('static/layuiadmin/layui/layui.js') }}"></script>
<script>
    layui.config({
        base: "/static/layuiadmin/" //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'user'], function () {
        var $ = layui.$
            , setter = layui.setter
            , admin = layui.admin
            , form = layui.form
            , router = layui.router()
            , search = router.search;

        //发送短信验证码
        admin.sendAuthCode({
            elem: '#LAY-user-forget-getsmscode'
            ,elemPhone: '#LAY-user-login-cellphone'
            ,elemVercode: '#LAY-user-login-vercode'
            ,ajax: {
                url:  "/admin/register/telCode"
            }
        });
        //过期跳转
        if (top != self) {
            top.location.href = '/admin/login';
        }

        form.render();

        //提交
        form.on('submit(LAY-user-reg-submit)', function (obj) {
            //请求登入接口
            admin.req({
                url: "{{ route('admin.register') }}"
                , method: 'POST'
                , data: obj.field
                , done: function (res) {
                    //请求成功后，写入 access_token
                    layui.data(setter.tableName, {
                        key: setter.request.tokenName
                        , value: res.data.access_token
                    });
                    if(res.code === 0){
                        layer.msg(res.msg, {
                            offset: '15px'
                            , icon: 1
                            , time: 1000
                        }, function () {
                            location.href = '/admin/index/index'; //后台主页
                        });
                    }else{
                        layer.msg(res.msg, {
                            offset: '15px'
                            , icon: 2
                            , time: 1000
                        }, function () {

                        });
                    }
                }
            });

        });
    });
</script>
</body>
</html>
