@extends('layouts.admin')
@section('header')
<link rel="stylesheet" href="{{ asset('css/style.css') }}" media="all">
<script src="{{ asset('js/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('js/echarts/qihuo.js') }}"></script>
@endsection
@section('title', '欢迎来到我的cms')
@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <!-- 搜索 -->
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                @if($is_admin === true)
                <div class="layui-inline">
                    <label class="layui-form-label">交易账号</label>
                    <div class="layui-input-inline">
                        <select name="admin_id" lay-filter="admin_id" lay-search="">
                            @foreach($transaction_users as $k=>$v)
                            <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                <div class="layui-inline">
                    <label class="layui-form-label">交易时间</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" id="times" placeholder=" ~ " name="times" value="{{ $start_time }} ~ {{ $end_time }}">
                    </div>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    交易手数
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="deal_num">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    当前净值
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="net_worth">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    最高净值
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="max_net_worth"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    最低净值
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="min_net_worth">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    最大回撤
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="max_retracement">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    初始资金
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="start_funds">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    期末资金
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="end_funds">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    累计出入金
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="go_out_funds">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4">
            <div class="layui-card">
                <div class="layui-card-header">
                    累计收益
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" id="overlay_income">0</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm12">
            <div class="layui-card">
                <div class="layui-card-header">
                    净值与风险仓位图表
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="charts" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-sm12">
            <div class="layui-card">
                <div class="layui-card-header">
                    客户权益与存取合计图表
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm6">
                            <div id="customer_rights" style="width: 100%;height:400px;"></div>
                        </div>
                        <div class="layui-col-sm6">
                            <div id="total_access_for_the_day" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'form', 'laydate', 'carousel'], function() {
        //console
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            carousel = layui.carousel,
            laydate = layui.laydate,
            element = layui.element;
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });
        //日期范围
        laydate.render({
            elem: '#times',
            range: '~'
            ,max: 0 //0天后
        });
        var theme = 'qihuo';
        var field = form.val("LAY-app-search");
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('charts'), theme);
        var myChart_customer_rights = echarts.init(document.getElementById('customer_rights'), theme);
        var myChart_total_access_for_the_day = echarts.init(document.getElementById('total_access_for_the_day'), theme);
        /**
         *获取数据
         * @constructor
         */
        function req() {
            if (field == undefined) {
                form.render();
                field = form.val("LAY-app-search");
            }
            admin.req({
                url: '/admin/index/mainAjax',
                data: field,
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        $('#deal_num').text(res.data['deal_num']);
                        $('#end_funds').text(res.data['end_funds']);
                        $('#go_out_funds').text(res.data['go_out_funds']);
                        $('#max_net_worth').text(res.data['max_net_worth']);
                        $('#max_retracement').text(res.data['max_retracement']);
                        $('#min_net_worth').text(res.data['min_net_worth']);
                        $('#net_worth').text(res.data['net_worth']);
                        $('#overlay_income').text(res.data['overlay_income']);
                        $('#start_funds').text(res.data['start_funds']);

                        // 使用刚指定的配置项和数据显示图表。
                        myChart.showLoading();
                        var option = {
                            title: {
                                text: '净值与风险仓位图表',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },
                            grid: {
                                bottom: 80
                            },
                            toolbox: {
                                feature: {
                                    dataZoom: {
                                        yAxisIndex: 'none'
                                    },
                                    restore: {},
                                    saveAsImage: {}
                                }
                            },
                            tooltip: {
                                trigger: 'axis',
                                axisPointer: {
                                    type: 'cross',
                                    animation: false,
                                    label: {
                                        backgroundColor: '#505765'
                                    }
                                }
                            },
                            legend: {
                                data: ['净值', '风险度'],
                                left: 10
                            },

                            xAxis: [{
                                type: 'category',
                                boundaryGap: false,
                                axisLine: {
                                    onZero: false
                                },
                                data: res.timeline.map(function(str) {
                                    return str.replace(' ', '\n');
                                })
                            }],
                            yAxis: [{
                                    name: '净值',
                                    type: 'value'
                                },
                                {
                                    name: '风险度',
                                    nameLocation: 'start',
                                    type: 'value',
                                    inverse: true
                                }
                            ],
                            series: [{
                                    name: '净值',
                                    type: 'line',
                                    animation: false,
                                    areaStyle: {},
                                    lineStyle: {
                                        width: 1
                                    },
                                    markArea: {
                                        silent: true,

                                    },
                                    data: res.worth
                                },
                                {
                                    name: '风险度',
                                    type: 'line',
                                    yAxisIndex: 1,
                                    animation: false,
                                    areaStyle: {},
                                    lineStyle: {
                                        width: 1
                                    },
                                    data: res.risk
                                }
                            ]
                        };
                        myChart.hideLoading();
                        myChart.setOption(option, true);

                        myChart_customer_rights.showLoading();
                        var option_customer_rights = {
                            title: {
                                text: '客户权益',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },
                            tooltip: {
                                trigger: 'axis',
                                axisPointer: {
                                    type: 'cross',
                                    animation: false,
                                    label: {
                                        backgroundColor: '#505765'
                                    }
                                }
                            },
                            xAxis: {
                                type: 'category',
                                data: res.timeline
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: [{
                                data: res.customer_rights,
                                type: 'bar',
                                showBackground: true,
                                // backgroundStyle: {
                                //     color: 'rgba(220, 220, 220, 0.8)'
                                // }
                            }]
                        };
                        myChart_customer_rights.hideLoading();
                        myChart_customer_rights.setOption(option_customer_rights, true);
                        myChart_total_access_for_the_day.showLoading();
                        var option_total_access_for_the_day = {
                            title: {
                                text: '存取合计',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },
                            tooltip: {
                                trigger: 'axis',
                                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                                    type: 'line' // 默认为直线，可选为：'line' | 'shadow'
                                }
                            },
                            legend: {
                                data: ['存取合计'],
                                left: 10
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            xAxis: {
                                type: 'category',
                                data: res.timeline
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: [{
                                name: '存取合计',
                                type: 'line',
                                smooth: true,
                                lineStyle: {
                                    width: 3,
                                    shadowColor: 'rgba(0,0,0,0.4)',
                                    shadowBlur: 10,
                                    shadowOffsetY: 10
                                },
                                data: res.total_access_for_the_day
                            }]
                        };
                        myChart_total_access_for_the_day.hideLoading();
                        myChart_total_access_for_the_day.setOption(option_total_access_for_the_day, true);
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
        }
        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            field = data.field;
            req();
        });
        // 默认查询
        $('.layuiadmin-btn-list').trigger("click");
    });
</script>
@endsection