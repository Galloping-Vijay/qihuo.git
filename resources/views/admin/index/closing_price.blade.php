@extends('layouts.admin')
@section('header')
<link rel="stylesheet" href="{{ asset('css/style.css') }}" media="all">
<script src="{{ asset('js/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('js/echarts/qihuo.js') }}"></script>
<style>
    #varietie_codes .layui-icon {
        position: static;
    }
</style>
@endsection
@section('title', '欢迎来到我的cms')
@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <!-- 搜索 -->
        @component('./layouts/public_search',['exchange_list' => $exchange_list,'exchange_id'=>$exchange_id,'varietie_codes'=>$varietie_codes,'varietie_code'=>$varietie_code,'start_time'=>$start_time,
        'end_time'=>$end_time,'transaction_users'=>$transaction_users,'is_admin'=>$is_admin,'admin_id'=>$admin_id])
        @endcomponent
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm12">
            <div class="layui-card">
                <div class="layui-card-header">
                    收盘价走势图
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="charts" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">
                    品种交易统计图
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="closingPriceBar" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">
                    品种交易比例图
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm6">
                            <div id="closingPrice3" style="width: 100%;height:400px;"></div>
                        </div>
                        <div class="layui-col-sm6">
                            <div id="closingPrice4" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">
                    买卖平开手数累计图
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="closingDot" style="width: 100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'form', 'laydate', 'carousel'], function() {
        //console
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            carousel = layui.carousel,
            laydate = layui.laydate,
            element = layui.element;
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });
        //日期范围
        laydate.render({
            elem: '#times',
            range: '~'
            ,max: 0 //0天后
        });
        // 监听交易所切换
        form.on('select(exchange_id)', function(data) {
            var html = '';
            $.get('/home/getVarietieList?exchange_id=' + data.value).done(function(res) {
                for (let i in res.data) {
                    if (i == 0) {
                        html += '<input type="radio" name="varietie_code" title="' + res.data[i] + '" value="' + res.data[i] + '" checked>';
                    } else {
                        html += '<input type="radio" name="varietie_code" title="' + res.data[i] + '" value="' + res.data[i] + '">';
                    }
                }
                $('#varietie_codes').html(html)
                form.render();
            });
        });
        var field = form.val("LAY-app-search");
        // 基于准备好的dom，初始化echarts实例
        var theme = 'qihuo';
        var myChart = echarts.init(document.getElementById('charts'), theme);
        var myChartClosingPriceBar = echarts.init(document.getElementById('closingPriceBar'), theme);
        var myChartclosingPrice3 = echarts.init(document.getElementById('closingPrice3'), theme);
        var myChartclosingPrice4 = echarts.init(document.getElementById('closingPrice4'), theme);
        var closingDot = echarts.init(document.getElementById('closingDot'), theme);

        /**
         *获取数据
         * @constructor
         */
        function req() {
            if (field == undefined) {
                form.render();
                field = form.val("LAY-app-search");
            }
            // 获取导入数据
            admin.req({
                url: '/getChart',
                data: field,
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        // 品种代码
                        var varietie_code_data = res.varietie_code;
                        //时间轴
                        var time_data = res.time_data;
                        var series_data = res.data;

                        myChart.showLoading();
                        var myChartOption = {
                            title: {
                                text: '收盘价走势图',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
                                data: varietie_code_data,
                                left: 10
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    saveAsImage: {}
                                }
                            },
                            xAxis: {
                                type: 'category',
                                boundaryGap: false,
                                data: time_data
                            },
                            yAxis: {
                                type: 'value',
                                smooth: true
                            },
                            series: series_data
                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChart.hideLoading();
                        myChart.setOption(myChartOption, true);
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
            // 获取逐日盯市
            admin.req({
                url: '/admin/index/closingPriceAjax',
                data: field,
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        myChartClosingPriceBar.showLoading();
                        var myChartClosingPriceBarOption = {
                            color: [
                                "#e01f54",
                                "#9b8bba",
                                "#87f7cf",
                                "#c12e34",
                                "#2ec7c9",
                                "#eb8146",
                                "#d95850",
                                "#a5e7f0",
                                "#4ea397",
                                "#d0648a",
                                "#d4a4eb",
                                "#d2f5a6",
                                "#76f2f2",
                                "#724e58",
                                "#4b565b",
                                "#cda819",
                            ],
                            // title: {
                            //     text: '品种交易统计图',
                            //     subtext: '数据来自中国期货市场监控中心',
                            //     left: 'center',
                            //     align: 'right'
                            // },
                            tooltip: {
                                trigger: 'axis',
                                axisPointer: {
                                    type: 'cross',
                                    crossStyle: {
                                        color: '#999'
                                    }
                                }
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    dataView: {
                                        show: true,
                                        readOnly: false
                                    },
                                    magicType: {
                                        show: true,
                                        type: ['line', 'bar']
                                    },
                                    restore: {
                                        show: true
                                    },
                                    saveAsImage: {
                                        show: true
                                    }
                                }
                            },
                            legend: {
                                data: res.legendArr
                            },
                            xAxis: [{
                                type: 'category',
                                data: res.xAxis,
                                axisPointer: {
                                    type: 'shadow'
                                }
                            }],
                            yAxis: [{
                                    type: 'value',
                                    name: '金额(¥)',
                                    axisLabel: {
                                        formatter: '{value} '
                                    }
                                },
                                {
                                    type: 'value',
                                    name: '手数/次数',
                                    axisLabel: {
                                        formatter: '{value} '
                                    }
                                }
                            ],
                            series: [{
                                    name: '累计盈亏',
                                    type: 'bar',
                                    data: res.data[0].data
                                },
                                {
                                    name: '手续费',
                                    type: 'bar',
                                    data: res.data[1].data
                                },
                                {
                                    name: '最大亏损',
                                    type: 'bar',
                                    data: res.data[2].data
                                },
                                {
                                    name: '最大盈利',
                                    type: 'bar',
                                    data: res.data[3].data
                                },
                                {
                                    name: '亏损次数',
                                    type: 'line',
                                    yAxisIndex: 1,
                                    data: res.data[4].data
                                },
                                {
                                    name: '盈利次数',
                                    type: 'line',
                                    yAxisIndex: 1,
                                    data: res.data[5].data
                                },
                                {
                                    name: '合计盈利',
                                    type: 'bar',
                                    data: res.data[6].data
                                },
                                {
                                    name: '合计亏损',
                                    type: 'bar',
                                    data: res.data[7].data
                                },
                                {
                                    name: '平均亏损',
                                    type: 'bar',
                                    data: res.data[8].data
                                },
                                {
                                    name: '平均盈利',
                                    type: 'bar',
                                    data: res.data[9].data
                                },
                                {
                                    name: '空头手数',
                                    type: 'line',
                                    yAxisIndex: 1,
                                    data: res.data[10].data
                                },
                                {
                                    name: '空头盈亏',
                                    type: 'bar',
                                    data: res.data[11].data
                                },
                                {
                                    name: '多头手数',
                                    type: 'line',
                                    yAxisIndex: 1,
                                    data: res.data[12].data
                                },
                                {
                                    name: '多头盈亏',
                                    type: 'bar',
                                    data: res.data[13].data
                                }
                            ]

                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChartClosingPriceBar.hideLoading();
                        myChartClosingPriceBar.setOption(myChartClosingPriceBarOption, true);

                        myChartclosingPrice3.showLoading();
                        var seriesData = [];
                        for (let i in res.codeNum) {
                            var vote = {};
                            vote.value = res.codeNum[i];
                            vote.name = res.xAxis[i];
                            seriesData.push(vote);

                        }
                        var myChartclosingPrice3Option = {
                            title: {
                                text: '品种交易次数比例图',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },
                            color: [
                                "#e01f54",
                                "#9b8bba",
                                "#87f7cf",
                                "#c12e34",
                                "#2ec7c9",
                                "#eb8146",
                                "#d95850",
                                "#a5e7f0",
                                "#4ea397",
                                "#d0648a",
                                "#d4a4eb",
                                "#d2f5a6",
                                "#76f2f2",
                                "#724e58",
                                "#4b565b",
                                "#cda819",
                            ],
                            tooltip: {
                                trigger: 'item',
                                formatter: '{a} <br/>{b} : {c} ({d}%)'
                            },
                            // legend: {
                            //     left: 'center',
                            //     top: 'bottom',
                            //     data: res.xAxis
                            // },
                            series: [{
                                name: '次数',
                                type: 'pie',
                                radius: [30, 110],
                                roseType: 'area',
                                data: seriesData
                            }]
                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChartclosingPrice3.hideLoading();
                        myChartclosingPrice3.setOption(myChartclosingPrice3Option, true);

                        myChartclosingPrice4.showLoading();
                        var myChartclosingPrice4Option = {
                            title: {
                                text: '品种交易盈亏比',
                                subtext: '数据来自中国期货市场监控中心',
                                left: 'center',
                                align: 'right'
                            },

                            tooltip: {
                                trigger: 'axis',
                                formatter: function(params) {
                                    var relVal = params[0].name;
                                    relVal += '<br/>' + '盈亏比：' + params[0].value + '<br/>' + '胜率：' + params[1].value + '%';
                                    return relVal;
                                }
                            },
                            legend: {
                                left: 10
                            },
                            xAxis: {
                                type: 'category',
                                data: res.xAxis
                            },
                            yAxis: [{
                                    type: 'value',
                                    name: '盈亏比',
                                    axisLabel: {
                                        formatter: '{value}'
                                    }
                                },
                                {
                                    type: 'value',
                                    name: '胜率(%)',
                                    axisLabel: {
                                        formatter: '{value}'
                                    }
                                }
                            ],
                            series: [{
                                data: res.deal_fee_scale,
                                type: 'bar'
                            }, {
                                data: res.win_rate,
                                yAxisIndex: 1,
                                type: 'line'
                            }]
                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChartclosingPrice4.hideLoading();
                        myChartclosingPrice4.setOption(myChartclosingPrice4Option, true);
                        // 买卖平开手数累计图
                        closingDot.showLoading();
                        var closingDotData = res.scatter_arr;
                        var closingDotOption = {
                            color: [
                                "#c12e34",
                                "#4ea397",
                            ],
                            legend: {
                                right: 10,
                                data: ['买开买平手数', '卖开卖平手数']
                            },
                            xAxis: {
                                data: res.xAxis,
                            },
                            yAxis: {},
                            series: [{
                                    name: '买开买平手数',
                                    symbolSize: function(data) {
                                        let point = data[1];
                                        if (point < 1) {
                                            return 0;
                                        } else if (point < 10) {
                                            return 20;
                                        } else if (point < 50) {
                                            return 30;
                                        } else if (point < 100) {
                                            return 40;
                                        } else {
                                            return 50;
                                        }
                                    },
                                    data: closingDotData['buy'],
                                    type: 'scatter',
                                    emphasis: {
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                var relVal = param.data[0] + "\r\n" + '买开：' + param.data[2] + '\r\n买平：' + param.data[3] + '\r\n合计：' + param.data[1];
                                                return relVal;
                                            },
                                            position: 'top',
                                            color: '#fff',
                                            fontWeight: 'bold',
                                            fontSize: 14,
                                            align: 'left',
                                            lineHeight: 16

                                        }
                                    },
                                },
                                {
                                    name: '卖开卖平手数',
                                    symbolSize: function(data) {
                                        let point = data[1];
                                        if (point < 1) {
                                            return 0;
                                        } else if (point < 10) {
                                            return 20;
                                        } else if (point < 50) {
                                            return 30;
                                        } else if (point < 100) {
                                            return 40;
                                        } else {
                                            return 50;
                                        }
                                    },
                                    data: closingDotData['sell'],
                                    type: 'scatter',
                                    emphasis: {
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                var relVal = param.data[0] + "\r\n" + '卖开：' + param.data[2] + '\r\n卖平：' + param.data[3] + '\r\n合计：' + param.data[1];
                                                return relVal;
                                            },
                                            position: 'top',
                                            color: '#fff',
                                            fontWeight: 'bold',
                                            fontSize: 14,
                                            align: 'left',
                                            lineHeight: 16
                                        }
                                    },
                                }
                            ]
                        };
                        closingDot.hideLoading();
                        closingDot.setOption(closingDotOption, true);
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
        }
        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            field = data.field;
            req();
        });
        // 默认查询
        $('.layuiadmin-btn-list').trigger("click");
    });
</script>
@endsection