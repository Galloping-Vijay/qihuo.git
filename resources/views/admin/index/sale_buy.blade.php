@extends('layouts.admin')
@section('header')
<link rel="stylesheet" href="{{ asset('css/style.css') }}" media="all">
<script src="{{ asset('js/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('js/echarts/qihuo.js') }}"></script>
<style>
    .layui-form-item .layui-form-checkbox[lay-skin=primary] {
        margin: auto;
    }
</style>
@endsection
@section('title', '欢迎来到我的cms')
@section('content')

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>卖平买平明细</legend>
</fieldset>

<div class="layui-tab" lay-filter="test">
    <ul class="layui-tab-title">
        <li class="layui-this">卖平买平单品类的细分图表</li>
        <li>买卖平开合计图表多个品类</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <div class="layui-fluid">
                <div class="layui-card">
                    <!-- 搜索 -->
                    <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                        <div class="layui-form-item">
                            @if($is_admin === true)
                            <div class="layui-inline">
                                <label class="layui-form-label">所属账号</label>
                                <div class="layui-input-inline">
                                    <select name="admin_id" lay-filter="admin_id" lay-search="">
                                        @foreach($transaction_users as $k=>$v)
                                        <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="layui-inline">
                                <label class="layui-form-label">交易时间</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" id="times" placeholder=" ~ " name="times" value="{{ $start_time }} ~ {{ $end_time }}">
                                </div>
                            </div>
                            <div class="layui-inline">
                                <label class="layui-form-label">所属交易所</label>
                                <div class="layui-input-inline">
                                    <select name="exchange_id" lay-filter="exchange_id_1"> 
                                        @foreach($exchange_list as $k=>$v)
                                        <option value="{{ $v['id'] }}" @if($v['id']==$exchange_id) selected @endif>{{ $v['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="layui-inline" pane="">
                                <label class="layui-form-label">品种组合</label>
                                <input lay-filter="varietie_combination_switch" type="checkbox" checked="" lay-skin="switch" lay-text="全选|反选">
                                <div class="layui-input-block" id="varietie_combination">
                                    @foreach($varietie_combination as $key=>$val)
                                    <input type="checkbox" name="varietie_combination[]" lay-skin="primary" class="varietie_combination_class" title="{{ $val }}" value="{{ $key }}" checked>
                                    @endforeach
                                </div>
                            </div>
                            <div class="layui-inline">
                                <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-sm12">
                        <div class="layui-card">
                            <div class="layui-card-header">
                                卖平买平单品类细分图
                            </div>
                            <div class="layui-card-body">
                                <div class="layui-row">
                                    <div class="layui-col-sm12">
                                        <div id="closingPriceBar" style="width: 100%;height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-tab-item">
            <div class="layui-fluid">
                <div class="layui-card">
                    <!-- 搜索 -->
                    <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                        <div class="layui-form-item">
                            @if($is_admin === true)
                            <div class="layui-inline">
                                <label class="layui-form-label">所属账号</label>
                                <div class="layui-input-inline">
                                    <select name="admin_id" lay-filter="admin_id" lay-search="">
                                        @foreach($transaction_users as $k=>$v)
                                        <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="layui-inline">
                                <label class="layui-form-label">交易时间</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" id="times2" placeholder=" ~ " name="times" value="{{ $start_time }} ~ {{ $end_time }}">
                                </div>
                            </div>
                            <div class="layui-inline">
                                <label class="layui-form-label">所属交易所</label>
                                <div class="layui-input-inline">
                                    <select name="exchange_id" lay-filter="exchange_id_2">
                                        @foreach($exchange_list as $k=>$v)
                                        <option value="{{ $v['id'] }}" @if($v['id']==$exchange_id) selected @endif>{{ $v['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="layui-inline" pane="">
                                <label class="layui-form-label">品种代码</label>
                                <input lay-filter="varietie_codes_switch" type="checkbox" checked="" lay-skin="switch" lay-text="全选|反选">
                                <div class="layui-input-block" id="varietie_codes">
                                    @foreach($varietie_codes as $key=>$val)
                                    <input type="checkbox" name="varietie_codes[]" lay-skin="primary" title="{{ $val }}" value="{{ $val }}" checked class="varietie_codes_class">
                                    @endforeach
                                </div>
                            </div>
                            <div class="layui-inline">
                                <button class="layui-btn layuiadmin-btn-list-2" lay-submit lay-filter="LAY-app-search2">
                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-sm12">
                        <div class="layui-card">
                            <div class="layui-card-header">
                                买卖平开合计图表
                            </div>
                            <div class="layui-card-body">
                                <div class="layui-row">
                                    <div class="layui-col-sm12">
                                        <div id="closingPriceBar2" style="width: 100%;height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'form', 'laydate', 'carousel'], function() {
        //console
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            carousel = layui.carousel,
            laydate = layui.laydate,
            element = layui.element;
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });
        //日期范围
        laydate.render({
            elem: '#times',
            range: '~'
            ,max: 0 //0天后
        });
        laydate.render({
            elem: '#times2',
            range: '~'
            ,max: 0 //0天后
        });
        // 设置宽度
        var width = $('.layui-field-title').width();
        $("#closingPriceBar2").css("width", width);
        var field = form.val("LAY-app-search");
        var field2 = form.val("LAY-app-search-2");

        // 基于准备好的dom，初始化echarts实例
        var theme = 'qihuo';
        // 监听交易所切换
        form.on('select(exchange_id_1)', function(data) {
            var html = '';
            $.get('/admin/index/saleBuyVarietie?exchange_id=' + data.value).done(function(res) {
                for (let i in res.data) {
                    html += '<input type="checkbox" name="varietie_combination[]" lay-skin="primary" class="varietie_combination_class" title="'+res.data[i]+'" value="' + i + '" checked>';
                }
                $('#varietie_combination').html(html)
                form.render();
            });
        });
        form.on('select(exchange_id_2)', function(data) {
            var html = '';
            $.get('/home/getVarietieList?exchange_id=' + data.value).done(function(res) {
                for (let i in res.data) {
                    html += '<input type="checkbox" name="varietie_codes[]" lay-skin="primary" title="' + res.data[i] + '" value="' + res.data[i] + '" checked class="varietie_codes_class">';
                }
                $('#varietie_codes').html(html)
                form.render();
            });
        });

        //监听指定开关，全选反选
        form.on('switch(varietie_combination_switch)', function(data) {
            if (this.checked == true) {
                $(".varietie_combination_class").prop("checked", true);
                form.render('checkbox');
            } else {
                $(".varietie_combination_class").prop("checked", false);
                form.render('checkbox');
            }
        });
        //监听指定开关，全选反选
        form.on('switch(varietie_codes_switch)', function(data) {
            if (this.checked == true) {
                $(".varietie_codes_class").prop("checked", true);
                form.render('checkbox');
            } else {
                $(".varietie_codes_class").prop("checked", false);
                form.render('checkbox');
            }
        });

        function req() {
            var myChartClosingPriceBar = echarts.init(document.getElementById('closingPriceBar'), theme);
            // 获取逐日盯市
            admin.req({
                url: '/admin/index/saleBuyAjax',
                data: field,
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        myChartClosingPriceBar.showLoading();
                        var myChartClosingPriceBarOption = {
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
                                data: ['累计盈亏', '手续费'],
                                left: 10
                            },
                            toolbox: {
                                show: true,
                                feature: {
                                    dataView: {
                                        show: true,
                                        readOnly: false
                                    },
                                    magicType: {
                                        show: true,
                                        type: ['line', 'bar']
                                    },
                                    restore: {
                                        show: true
                                    },
                                    saveAsImage: {
                                        show: true
                                    }
                                }
                            },
                            calculable: true,
                            xAxis: [{
                                type: 'category',
                                data: res.data.title
                            }],
                            yAxis: [{
                                type: 'value'
                            }],
                            series: [{
                                    name: '累计盈亏',
                                    type: 'bar',
                                    data: res.data.profit_loss,
                                    markPoint: {
                                        data: [{
                                                type: 'max',
                                                name: '最大值'
                                            },
                                            {
                                                type: 'min',
                                                name: '最小值'
                                            }
                                        ]
                                    },
                                    markLine: {
                                        data: [{
                                            type: 'average',
                                            name: '平均值'
                                        }]
                                    }
                                },
                                {
                                    name: '手续费',
                                    type: 'bar',
                                    data: res.data.fee,
                                }
                            ]
                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChartClosingPriceBar.hideLoading();
                        myChartClosingPriceBar.setOption(myChartClosingPriceBarOption, true);
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
        }

        function req2() {
            var myChartClosingPriceBar2 = echarts.init(document.getElementById('closingPriceBar2'), theme);
            // 获取逐日盯市
            admin.req({
                url: '/admin/index/saleBuyAjax',
                data: field2,
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                done: function(res) {
                    layer.closeAll('loading');
                    if (res.code === 0) {
                        myChartClosingPriceBar2.showLoading();
                        var myChartClosingPriceBar2Option = {
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
                                data: ['累计盈亏', '手续费'],
                                left: 10
                            },
                            toolbox: {
                                show: true,
                                feature: {
                                    dataView: {
                                        show: true,
                                        readOnly: false
                                    },
                                    magicType: {
                                        show: true,
                                        type: ['line', 'bar']
                                    },
                                    restore: {
                                        show: true
                                    },
                                    saveAsImage: {
                                        show: true
                                    }
                                }
                            },
                            calculable: true,
                            xAxis: [{
                                type: 'category',
                                data: res.z_res.title
                            }],
                            yAxis: [{
                                type: 'value'
                            }],
                            series: [{
                                    name: '累计盈亏',
                                    type: 'bar',
                                    data: res.z_res.profit_loss,
                                    markPoint: {
                                        data: [{
                                                type: 'max',
                                                name: '最大值'
                                            },
                                            {
                                                type: 'min',
                                                name: '最小值'
                                            }
                                        ]
                                    },
                                    markLine: {
                                        data: [{
                                            type: 'average',
                                            name: '平均值'
                                        }]
                                    }
                                },
                                {
                                    name: '手续费',
                                    type: 'bar',
                                    data: res.z_res.fee,
                                }
                            ]
                        };
                        // 使用刚指定的配置项和数据显示图表。
                        myChartClosingPriceBar2.hideLoading();
                        myChartClosingPriceBar2.setOption(myChartClosingPriceBar2Option, true);
                    } else {
                        layer.msg(res.msg, {
                            icon: 2
                        });
                    }
                }
            });
        }

        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            field = data.field;
            req();
        });
        form.on('submit(LAY-app-search2)', function(data) {
            field2 = data.field;
            console.log(field2)
            req2();
        });
        let index_alter = false;
        element.on('tab(test)', function(data) {
            if (data.index == 1 && index_alter === false) {
                index_alter = true;
                req2();
            }
        });
        // 默认查询
        $('.layuiadmin-btn-list').trigger("click");
    });
</script>
@endsection