@extends('layouts.admin')
@section('control_name', $control_name)
@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <!-- 搜索 -->
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                @if(isset($is_admin) && $is_admin === true)
                <div class="layui-inline">
                    <label class="layui-form-label">所属账号</label>
                    <div class="layui-input-inline">
                        <select name="admin_id" lay-filter="admin_id" lay-search="">
                            <option value="">全部</option>
                            @foreach($admin_user as $k=>$v)
                            <option @if($k==$admin_id) selected @endif value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                <div class="layui-inline">
                    <label class="layui-form-label">交易时间</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" id="times" placeholder=" ~ " name="times" value="">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">名称/代码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="keyword" placeholder="请输入" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">所属交易所</label>
                    <div class="layui-input-inline">
                        <select name="exchange_code">
                            <option value="">请选择交易所</option>
                            @foreach($exchange_list as $k=>$v)
                            <option value="{{ $v->code }}">{{ $v->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="layui-card-body" style="min-height: 600px">
            <!-- 按钮组 -->
            <!-- 表格 -->
            <table id="LAY-app-list" lay-filter="LAY-app-list"></table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    layui.config({
        base: "/static/layuiadmin/"
    }).extend({
        index: 'lib/index'
    }).use(['index', 'table', 'admin', 'laydate', 'upload'], function() {
        var table = layui.table,
            $ = layui.$,
            upload = layui.upload,
            admin = layui.admin,
            laydate = layui.laydate,
            form = layui.form;
        var control_name = $('meta[name="control_name"]').attr('content');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        laydate.render({
            elem: '#date_time'
        });
        //日期范围
        laydate.render({
            elem: '#times',
            range: '~'
            ,max: 0 //0天后
        });

        //表格数据
        table.render({
            elem: '#LAY-app-list',
            url: '/admin/' + control_name + '/index',
            method: 'post',
            where: {},
            headers: {
                'X-CSRF-TOKEN': csrf_token
            },
            cols: [
                [{
                    field: 'id',
                    width: 75,
                    title: 'ID',
                    align: 'center',
                    sort: true,
                }, {
                    field: 'admin_name',
                    title: '客户名称',
                    align: 'center',
                    minWidth: 80,
                }, {
                    field: 'deal_time',
                    title: '交易时间',
                    align: 'center',
                    width: 180,
                }, {
                    field: 'previous_day_balance',
                    title: '上日结存',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'customer_rights',
                    title: '客户权益',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'total_access_for_the_day',
                    title: '当日存取合计',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'actual_monetary_funds',
                    title: '实有货币资金',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'profit_loss_day',
                    title: '当日盈亏',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'not_currency_credit_amount',
                    title: '非货币充抵金额',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'total_royalties_of_the_day',
                    title: '当日总权利金',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'currency_credit_amount',
                    title: '货币充抵金额',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'day_handling_fee',
                    title: '当日手续费',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'frozen_funds',
                    title: '冻结资金',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'day_balance',
                    title: '当日结存',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'margin_occupation',
                    title: '保证金占用',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'available_funds',
                    title: '可用资金',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'risk',
                    title: '风险度',
                    align: 'center',
                    width: 80,
                }, {
                    field: 'margin_call',
                    title: '追加保证金',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'out_money_cny',
                    title: '出金（人民币）',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'enter_money_cny',
                    title: '入金（人民币）',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'deal_num',
                    title: '成交手数',
                    align: 'center',
                    width: 80,
                }, {
                    field: 'deal_turnover',
                    title: '成交额合计',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'deal_fee',
                    title: '成交手续费',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'deal_profit_loss',
                    title: '成交平仓盈亏',
                    align: 'center',
                    minWidth: 120,
                }, {
                    field: 'position_buy_position',
                    title: '持仓买持仓',
                    align: 'center',
                    width: 120,
                }, {
                    field: 'position_sell_position',
                    title: '持仓卖持仓',
                    align: 'center',
                    width: 120,
                }, {
                    field: 'position_profit_loss',
                    title: '持仓平仓盈亏',
                    align: 'center',
                    width: 120,
                }, {
                    field: 'position_trading_margin',
                    title: '持仓交易保证金',
                    align: 'center',
                    width: 120,
                }]
            ],
            page: true,
            limit: 20,
            done: function(res, curr, count) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
            }
        });
        //监听搜索
        form.on('submit(LAY-app-search)', function(data) {
            var field = data.field;
            //执行重载
            table.reload('LAY-app-list', {
                where: field
            });
        });
    });
</script>
@endsection