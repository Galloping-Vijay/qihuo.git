<?php

namespace App\Jobs;

use App\Models\ClosingData;
use App\Models\Exchange;
use App\Models\Message;
use App\Models\Varietie;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;

class XlsxJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file = null;

    /**
     * @var int
     */
    public $timeout = 86400;

    /**
     * 任务可尝试的次数
     *
     * @var int
     */
    public $tries = 3;

    /**
     * 任务失败前允许的最大异常数
     *
     * @var int
     */
    public $maxExceptions = 5;

    /**
     * XlsxJob constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function handle()
    {
        //        ini_set('memory_limit', '-1');
        //        set_time_limit(3600);
        $msg = '文件名：' . $this->file;
        $title = '管理端数据库写入';
        $text = '';
        Log::info($msg . "\n");
        if (!is_file($this->file)) {
            $msg = '没有找到该文件';
            $text .= ' ' . $msg;
            Log::info($msg . "\n");
            Message::writeMessage($title, $text, 1, 0, '', -1);
            return;
        }

        if (strpos($this->file, 'xlsx') === false && strpos($this->file, 'xls') === false && strpos($this->file, 'csv') === false) {
            $msg = '文件后缀不合格，不予处理';
            Log::info($msg . "\n");
            $text .= ' ' . $msg;
            Message::writeMessage($title, $text, 1, 0, '', -1);
            return;
        }
        $objReader = IOFactory::createReader('Xlsx');
        $objPHPExcel = $objReader->load($this->file);  //$filename可以是上传的表格，或者是指定的表格
        $objCount = $objPHPExcel->getSheetCount(); // sheet个数
        $data = [];  // 表格中获取的数据
        $date_time = date('Y-m-d');
        // 存在就覆盖，不存在添加
        $up_num = 0;
        $cr_num = 0;
        // 200条200条的存入数据库,防止数组过大
        $amount = 200;
        $times = 1;
        for ($i = 0; $i < $objCount; $i++) {
            $msg = '处理第' . $i . '个表格';
            Log::info($msg . "\n");
            $sheet = $objPHPExcel->getSheet($i);   //excel中的第i张sheet
            $highestRow = $sheet->getHighestRow();       // 取得总行数
            //循环读取excel表格，整合成数组。如果是不指定key的二维，就用$data[i][j]表示。
            for ($j = 2; $j <= $highestRow; $j++) {
                $oneData = [
                    'exchange_name' => $sheet->getCellByColumnAndRow(1, $j)->getValue(),
                    'exchange_code' => $sheet->getCellByColumnAndRow(2, $j)->getValue(),
                    'varietie_name' => $sheet->getCellByColumnAndRow(3, $j)->getValue(),
                    'varietie_code' => $sheet->getCellByColumnAndRow(4, $j)->getValue(),
                    'transaction' => $sheet->getCellByColumnAndRow(5, $j)->getValue(),
                    'closing_quotation' => $sheet->getCellByColumnAndRow(6, $j)->getValue(),
                    'deal_time' => date("Y-m-d", \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($sheet->getCellByColumnAndRow(7, $j)->getValue())),
                ];
                // 写入品种数据
                self::hasVarietie($oneData);
                if (!is_numeric($oneData['transaction'])) {
                    $oneData['transaction'] = 0.00;
                } else {
                    $oneData['transaction'] = round($oneData['transaction'], 2);
                }
                if (!is_numeric($oneData['closing_quotation'])) {
                    $oneData['closing_quotation'] = 0.00;
                } else {
                    $oneData['closing_quotation'] = round($oneData['closing_quotation'], 2);
                }
                $oneData['deal_time_stamp'] = strtotime($oneData['deal_time']);
                if (!empty($oneData['exchange_code'])) {
                    $oneData['created_at'] = $date_time;
                    $data[] = $oneData;
                }
                if (count($data) >= $amount) {
                    $msg = '...已累计' . $amount * $times . '条' . '，开始写入数据库';
                    Log::info($msg . "\n");
                    foreach ($data as $da) {
                        $info = ClosingData::query()
                            ->where('deal_time_stamp', $da['deal_time_stamp'])
                            ->where('exchange_code', $da['exchange_code'])
                            ->where('varietie_code', $da['varietie_code'])
                            ->first();
                        if ($info) {
                            $up_num++;
                            $info->transaction = $da['transaction'];
                            $info->closing_quotation = $da['closing_quotation'];
                            $res = $info->save();
                            if (!$res) {
                                $msg = '...写入失败第' . $j . '行';
                                Log::info($msg . "\n");
                            }
                        } else {
                            $cr_num++;
                            $res = ClosingData::query()->create($da);
                            if (!$res) {
                                $msg = '...写入失败第' . $j . '行';
                                Log::info($msg . "\n");
                            }
                        }
                    }
                    $msg = '...清空$data，继续执行';
                    Log::info($msg . "\n");
                    $times += 1;
                    $data = [];
                }
            }
            // 不到$amount条的情况
            if (count($data) > 0) {
                $msg = '...不到' . $amount . '条';
                Log::info($msg . "\n");
                foreach ($data as $da) {
                    $info = ClosingData::query()
                        ->where('deal_time_stamp', $da['deal_time_stamp'])
                        ->where('exchange_code', $da['exchange_code'])
                        ->where('varietie_code', $da['varietie_code'])
                        ->first();
                    if ($info) {
                        $up_num++;
                        $info->transaction = $da['transaction'];
                        $info->closing_quotation = $da['closing_quotation'];
                        $info->save();
                    } else {
                        $cr_num++;
                        ClosingData::query()->create($da);
                    }
                }
                $data = [];
            }
        }
        $msg = '...更新：' . $up_num . '条';
        $text .= ' ' . $msg;
        Log::info($msg . "\n");
        $msg = '...新增：' . $cr_num . '条';
        Log::info($msg . "\n");
        $text .= ' ' . $msg;
        Message::writeMessage($title, $text, 1, 0, '', -1);
        // 删除文件
        unlink($this->file);
        return;
    }

    /**
     * 自动添加交易所和品种
     */
    public static function hasVarietie(array $data = [])
    {
        $exchange_name = $data['exchange_name'];
        $exchange = Exchange::query()
            ->where('code', $data['exchange_code'])
            ->where(function ($query) use ($exchange_name) {
                $query->where('name', $exchange_name)->orWhere('settlement', $exchange_name);
            })
            ->first();
        if (!$exchange) {
            $exchange = Exchange::query()->create([
                'name' => $exchange_name,
                'settlement' => $exchange_name,
                'description' => $exchange_name,
                'code' => $data['exchange_code'],
            ]);
            Log::info('新建交易所:' . $exchange_name);
        }
        $varietie = Varietie::query()
            ->where('exchange_id', $exchange->id)
            ->where('code', $data['varietie_code'])
            ->where('title', $data['varietie_name'])
            ->first();
        if (!$varietie) {
            Varietie::query()->create([
                'exchange_id' => $exchange->id,
                'title' => $data['varietie_name'],
                'code' => $data['varietie_code'],
            ]);
            Log::info('新建品种:' . $data['varietie_name']);
        }
    }
}
