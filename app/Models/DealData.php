<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class DealData extends Model
{
    use TraitsModel;
    protected $table = 'deal_datas';
    protected $fillable = [
        'admin_name', 'query_time', 'deal_time', 'contract', 'exchange_code', 'exchange_num', 'buy_sell', 'speculation', 'final_price', 'num', 'turnover', 'open_flat', 'fee', 'profit_loss', 'created_at', 'deleted_at', 'deal_time_stamp'];
}
