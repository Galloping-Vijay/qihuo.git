<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class ClosingData extends Model
{
    use TraitsModel;

    protected $table = 'closing_datas';

    protected $fillable = [
        'deal_time', 'exchange_code', 'exchange_name', 'varietie_name', 'varietie_code', 'transaction', 'closing_quotation', 'created_at','deal_time_stamp'
    ];
}
