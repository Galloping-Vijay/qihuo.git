<?php
/**
 * Description:
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/08/27
 * Time: 14:28
 */

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

/**
 * This is the model class for table "Exchange".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property string $settlement
 * @property int $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * */
class Exchange extends Model
{
    use TraitsModel;

    protected $fillable = [
        'name', 'description', 'code', 'settlement', 'sort', 'created_at'
    ];
}
