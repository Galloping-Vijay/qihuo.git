<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['type', 'status', 'admin_id', 'admin_name', 'title', 'text', 'accept_admin_id', 'created_at'];

    /**
     * @var array
     */
    public static $statusList = [
        '1' => '已读',
        '2' => '未读'
    ];

    /**
     * @var array
     */
    public static $typeList = [
        '1' => '系统消息',
    ];

    /**
     * Description:通知消息写入
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/5
     * Time: 22:23
     * @param string $title
     * @param string $text
     * @param int $type
     * @param int $admin_id
     * @param string $admin_name
     * @param int $accept_admin_id
     */
    public static function writeMessage($title = '', $text = '', $type = 1, $admin_id = 0, $admin_name = '', $accept_admin_id = 0)
    {
        $model = new self();
        $model->title = $title;
        $model->text = $text;
        $model->type = $type;
        $model->type = $type;
        $model->admin_id = $admin_id;
        $model->admin_name = $admin_name;
        $model->accept_admin_id = $accept_admin_id;
        $model->save();
    }
}
