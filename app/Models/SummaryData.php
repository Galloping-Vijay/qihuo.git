<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class SummaryData extends Model
{
    protected $table = 'summary_datas';
    use TraitsModel;
    protected $fillable = [
        'admin_id', 'admin_name', 'query_time', 'deal_time', 'deal_time_stamp', 'fund_account', 'futures_company', 'previous_day_balance', 'customer_rights', 'total_access_for_the_day', 'actual_monetary_funds', 'profit_loss_day', 'not_currency_credit_amount', 'total_royalties_of_the_day', 'currency_credit_amount', 'day_handling_fee', 'frozen_funds', 'day_balance', 'margin_occupation', 'available_funds', 'risk', 'margin_call', 'out_money_cny', 'enter_money_cny', 'out_money_usd', 'enter_money_usd', 'exchange_name', 'transaction_Fees', 'reporting_fee', 'deal_num', 'deal_turnover', 'deal_fee', 'deal_profit_loss', 'position_buy_position', 'position_sell_position', 'position_profit_loss', 'position_trading_margin', 'created_at', 'deleted_at'];
}
