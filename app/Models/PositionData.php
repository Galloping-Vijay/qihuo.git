<?php
/*
 * @Description: 
 * @User: Vijay <1937832819@qq.com>
 * @Date: 2020-08-30 22:37:44
 */

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class PositionData extends Model
{
    use TraitsModel;
    protected $table = 'position_datas';
    protected $fillable = [
        'admin_name', 'query_time', 'deal_time', 'contract', 'exchange_code', 'exchange_num', 'buy_position', 'buy_average_price', 'sell_position', 'sell_average_price', 'settlement_price_yesterday', 'settlement_price_today', 'profit_loss', 'trading_margin', 'speculation', 'created_at', 'deleted_at','deal_time_stamp'];
}
