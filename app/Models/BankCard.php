<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class BankCard extends Model
{
    use TraitsModel;

    protected $fillable = [
        'transaction_users_id', 'bank', 'card', 'type', 'created_at'
    ];

}
