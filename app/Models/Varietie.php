<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;

class Varietie extends Model
{
    use TraitsModel;

    protected $fillable = [
        'exchange_id', 'status', 'title', 'description', 'code', 'transaction', 'closing_quotation', 'handling_fee', 'date_time', 'created_at'
    ];

    /**
     * Description:获取某交易所下的所有品种
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/5
     * Time: 20:42
     * @param int $exchange_id
     * @return \Illuminate\Support\Collection
     */
    public static function getVarietieList($exchange_id = 0)
    {
        $query = self::query();
        if ($exchange_id != 0) {
            $query->where('exchange_id', $exchange_id);
        }
        $codeList = $query->pluck('code');
        return $codeList;
    }

}
