<?php

namespace App\Models;

use App\Http\Traits\TraitsModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class TransactionUser extends Model
{
    use TraitsModel;

    protected $fillable = ['admin_id', 'username', 'password', 'ped', 'admin_name', 'money', 'qh_company', 'status', 'status_msg', 'created_at'];

    /**
     * 状态数组
     * @var array
     */
    public static $statusList = [
        1 => '已验证', 2 => '待验证', 3 => '验证失败',
    ];

    /**
     * Description:密码加密
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 14:58
     * @param string $string 加密字符串
     * @param string $skey 加盐
     * @return mixed
     */
    public function pwdEncode($string = '', $skey = 'qihuo')
    {
        $strArr = str_split(base64_encode($string));
        $strCount = count($strArr);
        foreach (str_split($skey) as $key => $value) {
            $key < $strCount && $strArr[$key] .= $value;
        }
        return str_replace(array('=', '+', '/'), array('O0O0O', 'o000o', 'oo00o'), join('', $strArr));
    }

    /**
     * Description:密码解密
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 14:59
     * @param string $string 解密字符串
     * @param string $skey 加盐
     * @return bool|string
     */
    public function pwdDecode($string = '', $skey = 'qihuo')
    {
        $strArr = str_split(str_replace(array('O0O0O', 'o000o', 'oo00o'), array('=', '+', '/'), $string), 2);
        $strCount = count($strArr);
        foreach (str_split($skey) as $key => $value) $key <= $strCount && $strArr[$key][1] === $value && $strArr[$key] = $strArr[$key][0];
        return base64_decode(join('', $strArr));
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/14
     * Time: 15:33
     * @param int $admin_id
     * @param bool $isCache
     * @return array|bool|\Illuminate\Database\Eloquent\Builder|Model|mixed|object|null
     */
    public static function getInfo($admin_id = 0, $isCache = true)
    {
        if (!$admin_id) return false;
        $key = 'transaction_users_' . $admin_id;
        $info = Cache::get($key);
        if (!$info || $isCache == false) {
            $info = self::query()->where('admin_id', $admin_id)->first();
            if (!$info) return false;
            $info = $info->toArray();
            Cache::put($key, $info);
        }
        return $info;
    }

}
