<?php
/**
 * Description:
 * Created by PhpStorm.
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/10/5
 * Time: 20:44
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\Varietie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use TraitResource;

    /**
     * Description:根据交易所获取品种
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/5
     * Time: 20:46
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getVarietieList(Request $request)
    {
        $exchange_id = $request->input('exchange_id', 0);
        $codeList = Varietie::getVarietieList($exchange_id);
        return $this->resJson(0, '获取成功', $codeList);
    }
}