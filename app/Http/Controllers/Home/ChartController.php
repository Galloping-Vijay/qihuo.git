<?php

/**
 * Description:
 * Created by PhpStorm.
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/9/6
 * Time: 16:53
 */

namespace App\Http\Controllers\Home;


use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\ClosingData;
use App\Models\Exchange;
use App\Models\SystemConfig;
use App\Models\Varietie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ChartController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        $today_start    = strtotime(date('Y-m-d', time()));
        // 默认查询昨天的
        $start_time = date('Y-m-d', $today_start - 86400 * 30);
        $end_time   = date('Y-m-d', $today_start);
        $timeRes = [
            'start_time' => $start_time,
            'end_time' => $end_time,
        ];
        View::share($timeRes);
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/7
     * Time: 22:41
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $res =  $this->closingPrice();
        $site_name     = SystemConfig::query()->where('key', 'site_name')->value('value');
        $res = array_merge($res, [
            'site_name' => $site_name
        ]);
        return view('home.chart.index', $res);
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/7
     * Time: 23:31
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getChart(Request $request)
    {
        $times          = $request->input('times', '');
        $exchange_id = $request->input('exchange_id', '');
        $varietie_codes = $request->input('varietie_codes', []);
        $varietie_code = $request->input('varietie_code', '');
        $query          = ClosingData::query();
        $today          = time();
        $today_start    = strtotime(date('Y-m-d', $today));
        if ($times != '') {
            $timeArr    = explode('~', $times);
            $start_time = trim($timeArr[0]);
            $end_time   = trim($timeArr[1]);
        } else {
            // 默认查询昨天的
            $start_time = date('Y-m-d', $today_start - 86400);
            $end_time   = date('Y-m-d', $today_start);
        }
        $timeRes = getTimeFormat('', $start_time, $end_time);
        $query->where('deal_time_stamp', '>=', $timeRes['start_time_str'])->where('deal_time_stamp', '<=', $timeRes['end_time_str']);
        if ($varietie_code != '') {
            $query->where('varietie_code', $varietie_code);
        } else if (!empty($varietie_codes)) {
            $query->whereIn('varietie_code', $varietie_codes);
        }
        $query->orderBy('deal_time_stamp', 'ASC');
        $res          = $query->get();
        $codeArr      = array_values(array_unique($res->pluck('varietie_code')->toArray()));
        $timeArr      = [];
        $timeStampArr = [];
        $dataArr      = []; //$dataArr[时间][商品]
        //x轴
        $i = $timeRes['start_time_str'];
        while ($i <= $timeRes['end_time_str']) {
            $timeArr[]      = date('Y-m-d', $i);
            $timeStampArr[] = $i;
            $i              += 86400;
        }
        // 构造数组
        foreach ($codeArr as $item) {
            foreach ($timeStampArr as $value) {
                $dataArr[$item][$value] = '0.00';
            }
        }
        foreach ($res as $re) {
            $dataArr[$re->varietie_code][$re->deal_time_stamp] = $re->transaction;
        }
        $resArr = [];
        foreach ($dataArr as $key => $val) {
            $resArrOne           = [];
            $resArrOne['name']   = $key;
            $resArrOne['type']   = 'line';
            $resArrOne['smooth'] = true;
            $resArrOne['stack']  = '成交价';
            $resArrOne['data']   = array_values($val);
            $resArr[]            = $resArrOne;
        }
        return $this->resJson(0, '获取成功', $resArr, [
            'varietie_code' => $codeArr,
            'time_data'     => $timeArr,
        ]);
    }
}
