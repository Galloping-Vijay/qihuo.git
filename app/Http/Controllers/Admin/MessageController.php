<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    use TraitResource;

    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        self::$model = Message::class;
        self::$controlName = 'message';
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/7/8
     * Time: 23:31
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $is_admin = $this->powerAdmin();
        if ($request->isMethod('post')) {
            $page = $request->input('page', 1);
            $limit = $request->input('limit', 10);
            $username = $request->input('username', '');
            $status = $request->input('status', '');
            $type = $request->input('type', '');
            $delete = $request->input('delete', 0);
            $title = $request->input('title', '');
            $query = self::$model::query();
            if ($username != '') {
                $query->where('admin_name', 'like', '%' . $username . '%');
            }
            if ($title != '') {
                $query->where('title', 'like', '%' . $title . '%');
            }
            if ($status != '') {
                $query->where('status', '=', $status);
            }
            if ($type != '') {
                $query->where('type', '=', $type);
            }
            if ($is_admin === false) {
                $user_id = Auth::id();
                $query->where(function ($query) use ($user_id) {
                    $query->where('accept_admin_id', $user_id)
                        ->orWhere('accept_admin_id', '=', 0);
                });
            }
            switch ($delete) {
                case '1':
                    $list = $query->onlyTrashed()->orderBy('id', 'desc')->get();
                    break;
                case '2':
                    $list = $query->withTrashed()->orderBy('id', 'desc')->get();
                    break;
                default:
                    $list = $query->orderBy('id', 'desc')->get();
                    break;
            }
            $res = self::getPageData($list, $page, $limit);
            return $this->resJson(0, '获取成功', $res['data'], ['count' => $res['count']]);
        }
        return view(
            'admin.' . self::$controlName . '.index',
            [
                'control_name' => self::$controlName,
                'status_list' => self::$model::$statusList,
                'type_list' => self::$model::$typeList,
            ]
        );
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/4
     * Time: 21:16
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $info = self::$model::find($request->id);
        if (empty($info)) {
            return $this->resJson(1, '没有该条记录');
        }
        $request->merge(
            [
                'admin_id' => Auth::id(),
                'admin_name' => Auth::user()->username
            ]
        );
        try {
            $res = $info->update($request->input());
            return $this->resJson(0, '操作成功', $res);
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/4
     * Time: 21:26
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $info = self::$model::find($id);
        $info->status = 1;
        $info->admin_id = Auth::id();
        $info->admin_name = Auth::user()->username;
        $info->save();
        return view(
            'admin.' . self::$controlName . '.detail',
            [
                'control_name' => self::$controlName,
                'info' => $info,
            ]
        );
    }

    /**
     * Description:标记已读
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/10/4
     * Time: 21:00
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function markRead(Request $request)
    {
        $id = $request->input('id', '');
        $ids = $request->input('ids', '');

        if ($id != '') {
            $ids = [$id];
        } elseif ($ids != '') {
        } else {
            return self::resJson(1, '请传入ID');
        }
        try {
            self::$model::whereIn('id', $ids)->update(
                [
                    'status' => 1,
                    'admin_id' => Auth::id(),
                    'admin_name' => Auth::user()->username
                ]
            );
            return $this->resJson(0, '操作成功');
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }
}
