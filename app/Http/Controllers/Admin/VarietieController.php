<?php

/**
 * Description:
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/08/27
 * Time: 14:35
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VarietieRequest;
use App\Http\Traits\TraitResource;
use App\Models\Varietie;
use Illuminate\Http\Request;
use App\Models\Exchange;

class VarietieController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        self::$model       = Varietie::class;
        self::$controlName = 'varietie';
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 20:46
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $where       = [];
            $title       = $request->input('title', '');
            $status      = $request->input('status', '');
            $delete      = $request->input('delete', 0);
            $exchange_id = $request->input('exchange_id', '');
            $page          = $request->input('page', 1);
            $limit         = $request->input('limit', 10);
            $query = self::$model::query();
            switch ($delete) {
                case '1':
                    $query->onlyTrashed();
                    break;
                case '2':
                    $query->withTrashed();
                default:
                    break;
            }
            if ($title != '') {
                $query->where(function ($query) use ($title) {
                    $query->where('varieties.title', 'like', '%' . $title . '%')
                        ->orWhere('varieties.code', 'like', '%' . $title . '%');
                });
            }
            if ($exchange_id != '') {
                $query->where('varieties.exchange_id', '=', $exchange_id);
            }
            if ($status != '') {
                $query->where('varieties.status', '=', $status);
            }
            $list = $query->join('exchanges', 'exchanges.id', '=', 'varieties.exchange_id')
                ->select('varieties.*', 'exchanges.name as exchange_name')
                ->where($where)
                ->orderBy('id', 'desc')
                ->get();
            $res = self::getPageData($list, $page, $limit);
            return $this->resJson(0, '获取成功', $res['data'], [
                'count' => $res['count']
            ]);
        }
        $exchange_list = Exchange::all();
        return view('admin.' . self::$controlName . '.index', [
            'control_name'  => self::$controlName,
            'delete_list'   => self::$model::$delete,
            'exchange_list' => $exchange_list,
        ]);
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 21:37
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $exchange_list = Exchange::all();
        return view(
            'admin.' . self::$controlName . '.create',
            [
                'control_name'  => self::$controlName,
                'exchange_list' => $exchange_list,
            ]
        );
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 22:09
     * @param VarietieRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(VarietieRequest $request)
    {
        $model = new self::$model;
        try {
            $model::create($request->input());
            return $this->resJson(0, '操作成功');
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 22:25
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $exchange_list = Exchange::all();
        $info          = self::$model::find($id);
        return view('admin.' . self::$controlName . '.edit', [
            'info'          => $info,
            'control_name'  => self::$controlName,
            'exchange_list' => $exchange_list,
        ]);
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 22:26
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function open(Request $request)
    {
        $info = self::$model::find($request->id);
        if (empty($info)) {
            return $this->resJson(1, '没有该条记录');
        }
        try {
            $res = $info->update($request->input());
            return $this->resJson(0, '操作成功', $res);
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 22:25
     * @param VarietieRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(VarietieRequest $request)
    {
        $info = self::$model::find($request->id);
        if (empty($info)) {
            return $this->resJson(1, '没有该条记录');
        }
        try {
            $res = $info->update($request->input());
            return $this->resJson(0, '操作成功', $res);
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }
}
