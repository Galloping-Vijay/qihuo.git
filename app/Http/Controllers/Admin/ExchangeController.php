<?php
/**
 * Description:
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/08/27
 * Time: 14:35
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\Exchange;

class ExchangeController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        self::$model       = Exchange::class;
        self::$controlName = 'exchange';
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/9/2
     * Time: 20:46
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $where  = [];
            $name   = $request->input('name', '');
            $status = $request->input('status', '');
            $delete = $request->input('delete', 0);
            if ($name != '') {
                $where[] = ['name', 'like', '%' . $name . '%'];
            }
            if ($status != '') {
                $where[] = ['status', '=', $status];
            }
            switch ($delete) {
                case '1':
                    $list = self::$model::onlyTrashed()->where($where)->orderBy('id', 'desc')->get();
                    break;
                case '2':
                    $list = self::$model::withTrashed()->where($where)->orderBy('id', 'desc')->get();
                    break;
                default:
                    $list = self::$model::where($where)->orderBy('id', 'desc')->get();
                    break;
            }
            return self::resJson(0, '获取成功', $list);
        }
        return view('admin.' . self::$controlName . '.index', [
            'control_name' => self::$controlName,
            'delete_list'  => self::$model::$delete,
        ]);
    }
}
