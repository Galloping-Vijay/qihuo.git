<?php
/*
 * @Description: 
 * @User: Vijay <1937832819@qq.com>
 * @Date: 2020-08-30 22:56:22
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\Admin;
use App\Models\DealData;
use App\Models\Exchange;
use App\Models\Varietie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DealDataController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        self::$model       = DealData::class;
        self::$controlName = 'deal_data';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $is_admin = $this->powerAdmin();
        if ($is_admin) {
            $admin_user = Admin::query()->pluck('username', 'id');
        } else {
            $admin_user = [Auth::id() => Auth::user()->username,];
        }
        if ($request->isMethod('post')) {
            $keyword       = $request->input('keyword', '');
            $delete        = $request->input('delete', 0);
            $times         = $request->input('times', '');
            $exchange_code = $request->input('exchange_code', '');
            $page          = $request->input('page', 1);
            $limit         = $request->input('limit', 10);
            $admin_id      = $request->input('admin_id', '');
            $query         = self::$model::query();
            $today         = time();
            $today_start   = strtotime(date('Y-m-d', $today));
            if (!$is_admin) {
                $admin_id = Auth::id();
            }
            if ($admin_id != '') {
                $query->where('admin_id', '=', $admin_id);
            }
            switch ($delete) {
                case '1':
                    $query->onlyTrashed();
                    break;
                case '2':
                    $query->withTrashed();
                    break;
                default:
                    break;
            }
            if ($exchange_code != '') {
                $codes = Varietie::query()
                    ->join('exchanges', 'exchanges.id', '=', 'varieties.exchange_id')
                    ->where('exchanges.code', $exchange_code)
                    ->pluck('varieties.code');
                $query->whereIn('exchange_code', $codes);
            }
            if ($keyword != '') {
                $query->where(function ($q) use ($keyword) {
                    $q->where('contract', '=', '%' . $keyword . '%')
                        ->orWhere('exchange_code', '=', $keyword);
                });
            }
            if ($times != '') {
                $timeArr    = explode('~', $times);
                $start_time = trim($timeArr[0]);
                $end_time   = trim($timeArr[1]);
            } else {
                // 默认查询昨天的
                $start_time = date('Y-m-d', $today_start - 86400 * 15);
                $end_time   = date('Y-m-d', $today_start);
            }
            $timeRes = getTimeFormat('', $start_time, $end_time);
            $query->where('deal_time_stamp', '>=', $timeRes['start_time_str'])->where('deal_time_stamp', '<=', $timeRes['end_time_str']);
            $list = $query->orderBy('id', 'desc')
                ->get();
            $res  = self::getPageData($list, $page, $limit);
            return $this->resJson(0, '获取成功', $res['data'], ['count' => $res['count']]);
        }
        $exchange_list = Exchange::all();
        return view('admin.' . self::$controlName . '.index', [
            'control_name'  => self::$controlName,
            'delete_list'   => self::$model::$delete,
            'exchange_list' => $exchange_list,
            'admin_user' => $admin_user,
            'is_admin' => $is_admin,
            'admin_id' => '',
        ]);
    }
}
