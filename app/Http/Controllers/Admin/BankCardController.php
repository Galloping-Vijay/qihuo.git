<?php
/**
 * Description:
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/08/27
 * Time: 14:35
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\BankCard;

class BankCardController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        self::$model = BankCard::class;
        self::$controlName = 'bank_card';
    }
}
