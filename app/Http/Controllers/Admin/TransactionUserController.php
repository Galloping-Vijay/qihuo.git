<?php
/**
 * Description:
 * User: Vijay <1937832819@qq.com>
 * Date: 2020/08/27
 * Time: 14:35
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\TraitResource;
use App\Models\Admin;
use App\Models\TransactionUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionUserController extends Controller
{
    use TraitResource;

    public function __construct()
    {
        self::$model = TransactionUser::class;
        self::$controlName = 'transaction_user';
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 10:46
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $is_admin = $this->powerAdmin();
        if ($request->isMethod('post')) {
            $page = $request->input('page', 1);
            $limit = $request->input('limit', 10);
            $keyword = $request->input('keyword', '');
            $delete = $request->input('delete', 0);
            $admin_id = $request->input('admin_id', '');
            $query = self::$model::query()->join('admins', 'admins.id', '=', 'transaction_users.admin_id');
            switch ($delete) {
                case '1':
                    $query->onlyTrashed();
                    break;
                case '2':
                    $query->withTrashed();
                    break;
                default:
                    break;
            }
            if ($is_admin === false) {
                $query->where('transaction_users.admin_id', Auth::id());
            } else {
                if ($admin_id != '') {
                    $query->where('transaction_users.admin_id', $admin_id);
                }
            }
            if ($keyword != '') {
                $query->where(
                    function ($q) use ($keyword) {
                        $q->where('transaction_users.username', '=', $keyword)->orWhere('transaction_users.admin_name', '=', $keyword);
                    }
                );
            }
            $list = $query->select('transaction_users.*', 'admins.username as admin_username')->orderBy('transaction_users.id', 'desc')->get();
            $res = self::getPageData($list, $page, $limit);
            return self::resJson(0, '获取成功', $res['data'], ['count' => $res['count']]);
        }
        if ($is_admin) {
            $admin_user = Admin::query()->pluck('username', 'id');
        } else {
            $admin_user = [Auth::id() => Auth::user()->username,];
        }
        return view(
            'admin.' . self::$controlName . '.index', [
                'delete_list'  => self::$model::$delete,
                'control_name' => self::$controlName,
                'admin_user'   => $admin_user,
                'status_list'  => TransactionUser::$statusList
            ]
        );
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 14:30
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $is_admin = $this->powerAdmin();
        if ($is_admin) {
            $admin_user = Admin::query()->pluck('username', 'id');
        } else {
            $admin_user = [Auth::id() => Auth::user()->username,];
        }
        return view('admin.' . self::$controlName . '.create', ['control_name' => self::$controlName, 'admin_user' => $admin_user,]);
    }

    public function store(Request $request)
    {
        $model = new self::$model;
        $password = $request->input('password');
        if (!$password) {
            return $this->resJson(1, '密码不能为空');
        }
        try {
            $request->merge([
                'ped' => $password, 'password' => $model->pwdEncode($password)
            ]);
            $model::create($request->input());
            return $this->resJson(0, '操作成功');
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 14:30
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $is_admin = $this->powerAdmin();
        if ($is_admin) {
            $admin_user = Admin::query()->pluck('username', 'id');
        } else {
            $admin_user = [
                Auth::id() => Auth::user()->username,
            ];
        }
        $info = self::$model::find($id);
        return view('admin.' . self::$controlName . '.edit', ['info' => $info, 'control_name' => self::$controlName, 'admin_user' => $admin_user,]);
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/11
     * Time: 15:44
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $info = self::$model::find($request->id);
        if (empty($info)) {
            return $this->resJson(1, '没有该条记录');
        }
        if ($request->has('password')) {
            $password = $request->input('password');
            if (!$password) {
                return $this->resJson(1, '密码不能为空');
            }
            $request->merge([
                'password' => $info->pwdEncode($password), 'ped' => $password,
            ]);
        }
        try {
            $res = $info->update($request->input());
            return $this->resJson(0, '操作成功', $res);
        } catch (\Exception $e) {
            return $this->resJson(1, $e->getMessage());
        }
    }
}
