<?php

namespace App\Observers;

use App\Models\TransactionUser;
use Illuminate\Support\Facades\Cache;

class TransactionUserObserver
{
    /**
     * Handle the transaction user "created" event.
     *
     * @param \App\Models\TransactionUser $transactionUser
     * @return void
     */
    public function created(TransactionUser $transactionUser)
    {
        //
    }

    /**
     * Handle the transaction user "updated" event.
     *
     * @param \App\Models\TransactionUser $transactionUser
     * @return void
     */
    public function updated(TransactionUser $transactionUser)
    {
        //
    }

    /**
     * Handle the transaction user "deleted" event.
     *
     * @param \App\Models\TransactionUser $transactionUser
     * @return void
     */
    public function deleted(TransactionUser $transactionUser)
    {
        $key = 'transaction_users_' . $transactionUser->admin_id;
        Cache::forget($key);
    }

    /**
     * Handle the transaction user "restored" event.
     *
     * @param \App\Models\TransactionUser $transactionUser
     * @return void
     */
    public function restored(TransactionUser $transactionUser)
    {
        //
    }

    /**
     * Handle the transaction user "force deleted" event.
     *
     * @param \App\Models\TransactionUser $transactionUser
     * @return void
     */
    public function forceDeleted(TransactionUser $transactionUser)
    {
        //
    }

    /**
     * Description:
     * User: Vijay <1937832819@qq.com>
     * Date: 2020/09/14
     * Time: 10:56
     * @param \App\Models\TransactionUser $transactionUser
     */
    public function saved(TransactionUser $transactionUser)
    {
        $transactionUser::getInfo($transactionUser->admin_id, false);
    }
}
