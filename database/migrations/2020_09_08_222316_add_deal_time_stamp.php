<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDealTimeStamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('closing_datas', function (Blueprint $table) {
            $table->integer('deal_time_stamp')->default('0')->after('deal_time')->comment('交易时间时间戳');
            $table->index('deal_time_stamp');
        });
        Schema::table('deal_datas', function (Blueprint $table) {
            $table->integer('deal_time_stamp')->default('0')->after('deal_time')->comment('交易时间时间戳');
            $table->index('deal_time_stamp');
        });
        Schema::table('position_datas', function (Blueprint $table) {
            $table->integer('deal_time_stamp')->default('0')->after('deal_time')->comment('交易时间时间戳');
            $table->index('deal_time_stamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
