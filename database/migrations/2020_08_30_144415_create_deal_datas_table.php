<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('deal_datas')) {
            Schema::create('deal_datas', function (Blueprint $table) {
                $table->bigIncrements('id')->comment(('期货成交汇总'));
                $table->string('admin_name', 80)->default('')->comment('客户名称');
                $table->timestamp('query_time', 0)->comment('查询时间')->nullable();
                $table->timestamp('deal_time', 0)->comment('交易时间')->nullable();
                $table->string('contract', 50)->default('')->comment('合约');
                $table->string('exchange_code', 50)->default('')->comment('交易所代码');
                $table->string('exchange_num', 50)->default('')->comment('合约数字');
                $table->string('buy_sell', 50)->default('买')->comment('买/卖');
                $table->string('speculation', 50)->default('-')->comment('投机/套保:1:投机，2：套保,0:-');
                $table->decimal('final_price', 15, 2)->default('0.00')->comment('成交价');
                $table->integer('num')->default('0')->comment('手数');
                $table->decimal('turnover', 15, 2)->default('0.00')->comment('成交额');
                $table->string('open_flat')->default('开')->comment('开/平');
                $table->decimal('fee', 15, 2)->default('0.00')->comment('手续费');
                $table->decimal('profit_loss', 15, 2)->default('0.00')->comment('平仓盈亏');

                $table->index(['admin_name', 'created_at', 'open_flat', 'buy_sell']);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_datas');
    }
}
