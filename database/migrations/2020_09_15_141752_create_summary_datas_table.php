<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummaryDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('summary_datas')) {
            Schema::create('summary_datas', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('逐日盯市汇总数据');
                $table->integer('admin_id')->default('0')->comment('后台管理员ID');
                $table->string('admin_name', 100)->default('')->comment('客户名称');
                $table->timestamp('query_time', 0)->comment('查询时间')->nullable();
                $table->timestamp('deal_time', 0)->comment('交易时间')->nullable();
                $table->integer('deal_time_stamp')->default('0')->comment('交易时间时间戳');
                $table->decimal('fund_account', 15, 2)->default('0.00')->comment('客户期货期权内部资金账户');
                $table->string('futures_company', 100)->default('')->comment('期货公司名称');
                $table->decimal('previous_day_balance', 15, 2)->default('0.00')->comment('上日结存');
                $table->decimal('customer_rights', 15, 2)->default('0.00')->comment('客户权益');
                $table->decimal('total_access_for_the_day', 15, 2)->default('0.00')->comment('当日存取合计');
                $table->decimal('actual_monetary_funds', 15, 2)->default('0.00')->comment('实有货币资金');
                $table->decimal('profit_loss_day', 15, 2)->default('0.00')->comment('当日盈亏');
                $table->decimal('not_currency_credit_amount', 15, 2)->default('0.00')->comment('非货币充抵金额');
                $table->decimal('total_royalties_of_the_day', 15, 2)->default('0.00')->comment('当日总权利金');
                $table->decimal('currency_credit_amount', 15, 2)->default('0.00')->comment('货币充抵金额');
                $table->decimal('day_handling_fee', 15, 2)->default('0.00')->comment('当日手续费');
                $table->decimal('frozen_funds', 15, 2)->default('0.00')->comment('冻结资金');
                $table->decimal('day_balance', 15, 2)->default('0.00')->comment('当日结存');
                $table->decimal('margin_occupation', 15, 2)->default('0.00')->comment('保证金占用');
                $table->decimal('available_funds', 15, 2)->default('0.00')->comment('可用资金');
                $table->string('risk')->default('')->comment('风险度');
                $table->decimal('margin_call', 15, 2)->default('0.00')->comment('追加保证金');
                $table->decimal('out_money_cny', 15, 2)->default('0.00')->comment('出金（人民币）');
                $table->decimal('enter_money_cny', 15, 2)->default('0.00')->comment('入金（人民币）');
                $table->decimal('out_money_usd', 15, 2)->default('0.00')->comment('出金（美元）');
                $table->decimal('enter_money_usd', 15, 2)->default('0.00')->comment('入金（美元）');
                $table->string('exchange_name')->default('')->comment('交易所（其它）');
                $table->decimal('transaction_fees', 15, 2)->default('0.00')->comment('交易手续费（其它）');
                $table->decimal('reporting_fee', 15, 2)->default('0.00')->comment('中金所申报费（其它）');
                $table->integer('deal_num')->default('0')->comment('成交手数');
                $table->decimal('deal_turnover', 15, 2)->default('0.00')->comment('成交额合计');
                $table->decimal('deal_fee', 15, 2)->default('0.00')->comment('成交手续费');
                $table->decimal('deal_profit_loss', 15, 2)->default('0.00')->comment('成交平仓盈亏');
                $table->integer('position_buy_position')->default('0')->comment('持仓买持仓');
                $table->integer('position_sell_position')->default('0')->comment('持仓卖持仓');
                $table->decimal('position_profit_loss', 15, 2)->default('0.00')->comment('持仓平仓盈亏');
                $table->decimal('position_trading_margin', 15, 2)->default('0.00')->comment('持仓交易保证金');
                $table->index(['admin_id', 'deal_time_stamp']);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_datas');
    }
}
