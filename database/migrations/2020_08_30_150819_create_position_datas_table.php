<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('position_datas')) {
            Schema::create('position_datas', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('期货持仓汇总');
                $table->string('admin_name', 100)->default('')->comment('客户名称');
                $table->timestamp('query_time', 0)->comment('查询时间')->nullable();
                $table->timestamp('deal_time', 0)->comment('交易时间')->nullable();
                $table->string('contract', 50)->default('')->comment('合约');
                $table->string('exchange_code', 50)->default('')->comment('交易所代码');
                $table->string('exchange_num', 50)->default('')->comment('合约数字');
                $table->integer('buy_position')->default('0')->comment('买持仓');
                $table->decimal('buy_average_price',15,2)->default('0.00')->comment('买均价');
                $table->integer('sell_position')->default('0')->comment('卖持仓');
                $table->decimal('sell_average_price',15,2)->default('0.00')->comment('卖均价');
                $table->decimal('settlement_price_yesterday',15,2)->default('0.00')->comment('昨结算价');
                $table->decimal('settlement_price_today',15,2)->default('0.00')->comment('今结算价');
                $table->decimal('profit_loss', 15, 2)->default('0.00')->comment('平仓盈亏');
                $table->decimal('trading_margin', 15, 2)->default('0.00')->comment('交易保证金');
                $table->string('speculation', 50)->default('-')->comment('投机/套保:1:投机，2：套保,0:-');

                $table->timestamps();
                $table->index(['admin_name', 'created_at']);
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_datas');
    }
}
