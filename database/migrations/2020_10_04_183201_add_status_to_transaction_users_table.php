<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToTransactionUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_users', function (Blueprint $table) {
            $table->tinyInteger('status')->default('2')->after('password')->comment('状态，1：已验证，2：待验证，3：验证失败');
            $table->string('status_msg',255)->default('')->after('status')->comment('状态信息');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_users', function (Blueprint $table) {
            //
        });
    }
}
