<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToDealTimeStamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deal_datas', function (Blueprint $table) {
            $table->integer('admin_id')->default('0')->after('admin_name')->comment('后台管理员ID');
            $table->index(['admin_id', 'deal_time_stamp']);
        });
        Schema::table('position_datas', function (Blueprint $table) {
            $table->integer('admin_id')->default('0')->after('admin_name')->comment('后台管理员ID');
            $table->index(['admin_id', 'deal_time_stamp']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deal_time_stamp', function (Blueprint $table) {
            //
        });
    }
}
