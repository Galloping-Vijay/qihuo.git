<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bank_cards')) {
            Schema::create('bank_cards', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('用户结算账户表');
                $table->tinyInteger('transaction_users_id')->default('0')->comment('账户表id');
                $table->string('bank')->default('')->comment('银行名称');
                $table->string('card')->default('')->comment('账户');
                $table->tinyInteger('type')->default('0')->comment('币种,0:人民币');
                $table->index('transaction_users_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_cards');
    }
}
