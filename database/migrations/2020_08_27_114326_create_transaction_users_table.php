<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaction_users')) {
            Schema::create('transaction_users', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('交易用户表');
                $table->tinyInteger('admin_id')->default('0')->comment('交易账号对应的管理账号');
                $table->string('username')->default('')->comment('用户名');
                $table->string('password')->default('')->comment('密码');
                $table->string('admin_name')->default('')->comment('客户名');
                $table->decimal('money',15,2)->default('0.00')->comment('客户期货期权内部资金账户');
                $table->string('qh_company')->default('')->comment('期货公司名称');
                $table->index('admin_name');
                $table->index('admin_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_users');
    }
}
