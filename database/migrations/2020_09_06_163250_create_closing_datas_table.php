<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClosingDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('closing_datas')) {
            Schema::create('closing_datas', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('收盘数据表');
                $table->timestamp('deal_time', 0)->comment('交易时间')->nullable();
                $table->string('exchange_code', 50)->default('')->comment('交易所代码');
                $table->string('exchange_name', 50)->default('')->comment('交易所名称');
                $table->string('varietie_name',100)->default('')->comment('品种名称');
                $table->string('varietie_code',100)->default('')->comment('品种代码');
                $table->decimal('transaction',15,2)->default('0.00')->comment('成交价');
                $table->decimal('closing_quotation',15,2)->default('0.00')->comment('结算价');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closing_datas');
    }
}
