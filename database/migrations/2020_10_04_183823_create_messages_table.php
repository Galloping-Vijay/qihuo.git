<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('消息通知');
            $table->tinyInteger('type')->default('1')->comment('消息类型1:系统');
            $table->tinyInteger('status')->default('2')->comment('消息状态,1:已读，2：未读');
            $table->integer('admin_id')->default('0')->comment('操作用户id');
            $table->string('admin_name')->default('')->comment('操作用户昵称');
            $table->string('title')->default('')->comment('标题');
            $table->text('text')->comment('消息内容');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
