<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVarietiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('varieties')) {
            Schema::create('varieties', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('品种表');
                $table->integer('exchange_id')->default('0')->comment('交易所id');
                $table->tinyInteger('status')->default('1')->comment('状态，1：正常，2:关闭');
                $table->string('title',255)->default('')->comment('标题');
                $table->string('description')->default('')->comment('描述');
                $table->string('code',100)->default('')->comment('品种代码');
                $table->decimal('transaction',15,2)->default('0.00')->comment('交易价');
                $table->decimal('closing_quotation',15,2)->default('0.00')->comment('收盘价');
                $table->decimal('handling_fee',15,2)->default('0.00')->comment('手续费');
                $table->timestamp('date_time', 0)->comment('品种日期')->nullable();
                $table->index('exchange_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('varieties');
    }
}
