<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('exchanges')) {
            Schema::create('exchanges', function (Blueprint $table) {
                $table->bigIncrements('id')->comment('交易所');
                $table->string('name',100)->default('')->comment('名称');
                $table->string('description')->default('')->comment('描述');
                $table->string('code',100)->default('')->comment('代码');
                $table->string('settlement',100)->default('')->comment('结算名称');
                $table->boolean('sort')->default(0)->comment('排序');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
}
