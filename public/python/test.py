# -*- coding:utf8 -*-

# 优化格式化化版本
import os
from shibie import baiduBce
from shibie import tx


def returnFilePath(path, file):
    '''
    返回文件路径
    :param path:路径
    :param file:文件名
    :return:
    '''
    # 是否存在目录
    if not os.path.exists(path):
        os.makedirs(path)
    return path+'/' + file

path = './code_img'
res =tx.get_code(path)
print(res)

# path = './code_img'
# file = 'logo_code.jpg'

# tmp_file = returnFilePath(path, file)
# print(tmp_file)
