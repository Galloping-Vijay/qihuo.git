# coding=utf-8
import xlwt

# 创建一个工作簿
myBook = xlwt.Workbook()
# 创建一个工作表格
mySheet = myBook.add_sheet('测试表格')
# 写入数据
# mySheet.write(i,j,value,style)
# i列、j行、value值、style样式，注意，行和列都是从0开始算的
# 创建数据格式，写入数据
myStyle = xlwt.easyxf('font:name Times New Roman,color-index red,bold on')
# 写数据的时候可以用这个格式
mySheet.write(3, 0, 'abcd', myStyle)
# 合并行4到6的列0到1，里面的内容为'品种'
mySheet.write_merge(4, 6, 0, 1, '品种', myStyle)
# 写入A3，数值等于1
mySheet.write(2, 0, 1)
# 写入B3，数值等于1
mySheet.write(2, 1, 1)
# 写入C3，数值等于2
mySheet.write(2, 2, xlwt.Formula("A3+B3"))
# 保存
myBook.save('test.xls')
